$(document).ready(function(){
	function filter(container , inputElement){
		var container = $(container);

		console.log(container);
		// find the elements within given container
		container.each(function(){
			var row = $(this);
			// hide all elements in container
			row.hide();

			// filter through elements from parameter with text
			var match = false;

			var element = $(row).children();
			// console.log(element.text().trim());
			if( element.text().trim().toLowerCase().indexOf($(inputElement).val().toLowerCase()) != -1){
			 match = true;
			}

			// show elements with match 
			if(match){
				row.show();
			}
		});
	}

	$('#filter-input').keyup(function(){
		filter('.filtered-line' , '#filter-input');	
	});
});

