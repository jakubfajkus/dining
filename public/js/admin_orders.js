 $(document).ready(function(){
 	$('.settings select').change(function()
 	{
 		$('.line').hide();
 		updateByMealType()
 		updateByUser();
 	});

 	$('.settings #user').keyup(function(){
 		$('.line').hide();
 		if( $('.settings select').val() == 0 )
 		{
 			$('.line').show();
 			updateByUser(); 
 		}
 		else
 		{
	 		updateByMealType()
	 		updateByUser(); 
 		}						
 	});

 	$('.settings #reset').click(function(){
 		$('.line').each(function(){
			$(this).show();
 		});

		$('.settings .meal-types-select select option').first().prop('selected' , true);
		$('.settings input').val('');
 	});

 	$('form').click(function(e)
 	{
 		e.preventDefault();

 		var orderId = parseInt( $(this).children('input[name=id]').val() );
 		var currentButton = $(this).children('button');

 		$.ajax({            
		    type: 'POST',
		    url:'order',
		    data: {_method: 'post', _token :$('input[type=hidden]').val() , id : orderId},
		    success:function(result){
		    	$(currentButton).removeClass('btn-primary').addClass('disabled').prop('value' , 'Vyzvednuto');
		    	$('.settings input').focus();
		    }
		});

 	});

 	function updateByMealType () {
 		var selectedValue = $('.settings select').val().trim();

 		$('.line').each(function(){
 			var currentLine = $(this);

 			$(currentLine).children('.meal-type-name').each(function(){
	 			var currentParagraphText = $(this).children('p').text().trim();

	 			if(selectedValue != '')
	 			{
	 				if(selectedValue == currentParagraphText)
	 				{
	 					currentLine.show();
	 				}
	 				else
	 				{
	 					currentLine.hide();
	 				}
	 			}
	 			else
	 			{
	 				currentLine.show();
	 			} 				
 			}); 			
 		});
 	}

 	function updateByUser() 
 	{
 		var userInput = $('.settings input').val().toLowerCase();
 		
 		$('.line').each(function(){
 			var currentLine = $(this);
	 		var currentIdParagraphText = $(currentLine).children('.user-id').children('p').text().trim().toLowerCase();
	 		var currentEmailParagraphText = $(currentLine).children('.user-email').children('p').text().trim().toLowerCase();
	 		var currentNameParagraphText = $(currentLine).children('.user-name').children('p').text().trim().toLowerCase();

		 	if(userInput != '')
		 	{
		 		if(  (currentEmailParagraphText.indexOf(userInput) == -1)  && (currentNameParagraphText.indexOf(userInput) == -1) && (! (userInput == currentIdParagraphText)) )
		 		{
			 		currentLine.hide();		
		 		}
		 	}		 				
 		}); 		
 	}
});


