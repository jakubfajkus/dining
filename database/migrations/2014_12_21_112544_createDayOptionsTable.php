<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('day_options', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('day_id')->unsigned();
			$table->integer('meal_id')->unsigned();
			$table->integer('meal_type_id')->unsigned();
			$table->text('comment')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('day_options');
	}

}
