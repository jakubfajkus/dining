<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankAPICOlumnsToTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transactions', function($table)
		{
			$table->string('account')->nullable();
			$table->string('bank_code')->nullable();
			$table->string('bank_name')->nullable();
			$table->string('constant_symbol')->nullable();
			$table->string('variable_symbol')->nullable();
			$table->string('specific_symbol')->nullable();
			$table->string('user_identification')->nullable();
			$table->string('message')->nullable();
			$table->string('type')->nullable();
			$table->string('transaction_comment')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transactions', function($table)
		{
			$table->dropColumn('account');
			$table->dropColumn('bank_code');
			$table->dropColumn('bank_name');
			$table->dropColumn('constant_symbol');
			$table->dropColumn('variable_symbol');
			$table->dropColumn('specific_symbol');
			$table->dropColumn('user_identification');
			$table->dropColumn('message');
			$table->dropColumn('type');
			$table->dropColumn('transaction_comment');
		});		
	}

}
