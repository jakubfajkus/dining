<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMealsMealTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('meals_meal_types', function ($table)
		{
			$table->foreign('meal_id')->references('id')->on('meals');
			$table->foreign('meal_type_id')->references('id')->on('meal_types');

			$table->primary(['meal_id','meal_type_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('meals_meal_types', function ($table)
		{
			$table->dropForeign('meals_meal_types_meal_id_foreign');
			$table->dropForeign('meals_meal_types_meal_type_id_foreign');
		});
	}

}
