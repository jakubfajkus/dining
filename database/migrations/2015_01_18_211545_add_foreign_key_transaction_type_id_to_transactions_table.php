<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyTransactionTypeIdToTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transactions', function ($table)
		{
			$table->foreign('transaction_type_id')->references('id')->on('transaction_types');	
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transactions', function ($table)
		{
			$table->dropForeign('transactions_transaction_type_id_foreign');
		});
	}

}
