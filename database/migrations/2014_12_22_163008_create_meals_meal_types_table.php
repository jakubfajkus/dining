<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealsMealTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('meals_meal_types', function(Blueprint $table)
		{
			$table->integer('meal_id')->unsigned();
			$table->integer('meal_type_id')->unsigned();
			// $table->primary(['meal_id','meal_type_id']);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('meals_meal_types');
	}

}
