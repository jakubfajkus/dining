<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToDayOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('day_options', function ($table)
		{
			$table->foreign('meal_id')->references('id')->on('meals');
			$table->foreign('day_id')->references('id')->on('days');
			$table->foreign('meal_type_id')->references('id')->on('meal_types');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('day_options', function ($table)
		{
			$table->dropForeign('day_options_meal_id_foreign');
			$table->dropForeign('day_options_day_id_foreign');
			$table->dropForeign('day_options_meal_type_id_foreign');
		});
	}

}
