<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteSoftDeletes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('days', function($table){
			$table->dropSoftDeletes();
		});

		Schema::table('day_options', function($table){
			$table->dropSoftDeletes();
		});

		Schema::table('meals', function($table){
			$table->dropSoftDeletes();
		});

		Schema::table('meals_meal_types', function($table){
			$table->dropSoftDeletes();
		});

		Schema::table('meal_types', function($table){
			$table->dropSoftDeletes();
		});

		Schema::table('orders', function($table){
			$table->dropSoftDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('days', function($table){
			$table->softDeletes();
		});

		Schema::table('day_options', function($table){
			$table->softDeletes();
		});

		Schema::table('meals', function($table){
			$table->softDeletes();
		});

		Schema::table('meals_meal_types', function($table){
			$table->softDeletes();
		});

		Schema::table('meal_types', function($table){
			$table->softDeletes();
		});

		Schema::table('orders', function($table){
			$table->softDeletes();
		});
	}

}
