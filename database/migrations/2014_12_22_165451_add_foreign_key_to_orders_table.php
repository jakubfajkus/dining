<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function ($table)
		{
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('day_option_id')->references('id')->on('day_options');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function ($table)
		{
			$table->dropForeign('orders_user_id_foreign');
			$table->dropForeign('orders_day_option_id_foreign');
		});
	}

}

