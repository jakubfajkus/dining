@extends('master')

@section('title')
	{{ 'Přihlášení' }}
@stop

@section('content')	
	@if( $errors->has('auth') )
		{!! $errors->first('auth' , '<span class="help-block"> :message </span>') !!}
	@endif

	{!! Form::open(array('url' => '/auth/login')) !!}
		<div class="form-group {{ ($errors->has('dining_room_id'))? 'has-error' : '' }}">
			{!! Form::label('dining_room_id' , 'Číslo jídelny')!!}
			{!! Form::text('dining_room_id' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('dining_room_id' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('id') || $errors->has('userDoesNotExist') )? 'has-error' : '' }}">
			{!! Form::label('id' , 'Číslo strávníka')!!}
			{!! Form::text('id' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('id' , '<span class="help-block"> :message </span>') !!}
			{!! $errors->first('userDoesNotExist' , '<span class="help-block"> :message </span>') !!}
		</div>
				
		<div class="form-group {{ ($errors->has('id') || $errors->has('userDoesNotExist') )? 'has-error' : '' }}">
			{!! Form::label('password' , 'Heslo')!!}
			{!! Form::password('password' ,['class' => 'form-control']) !!}
			{!! $errors->first('password' , '<span class="help-block"> :message </span>') !!}
			{!! $errors->first('userDoesNotExist' , '<span class="help-block"> :message </span>') !!}
		</div>

		{!! Form::submit('Přihlásit' , ['class' => 'btn btn-primary']) !!}	

	{!! Form::close() !!}

@stop