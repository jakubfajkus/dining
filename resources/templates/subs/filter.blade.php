@section('scripts')
	<script type="text/javascript" charset="utf-8" src="/js/filter.js"></script>
@stop

<div class="filter">
	<div class="form-inline row">
		<div class="col-md-4 from-group">
			<label for="filter-input">Hledat:</label>
			<input type="text" id='filter-input' class="form-control">
		</div>
	</div>
</div>