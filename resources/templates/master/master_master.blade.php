@extends('master')

@section('navigation')
	<li>{!! link_to_action('MasterController@index' ,'Přehled') !!}</li>
	<li>{!! link_to_action('MasterController@create', 'Přidat novou jídelnu') !!}</li>
	<li>{!! link_to('auth/logout' 					, 'Odhlásit') !!}</li>
@endsection