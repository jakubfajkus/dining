@extends('master/master_master')

@section('content')

	@if(isset($password))
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">Error:</span>
			Byla vytvořena nová jídelna s číslem {{ $diningRoomId }}
			Heslo pro administrátora jídelny je: {{ $password }}, heslo bude vyžadováno při přihlašování do jídelny, poté může být změneno:
		</div>
	@endif
	@forelse ($data as $diningRoom)
		<div class="row">
			<div class="col-md-6">
				{{ $diningRoom->name }}
			</div>
			
			<div class="col-md-1">
					<a href={!! action('MasterController@edit' , $diningRoom->id) !!}><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>
			</div>

			<div class="col-md-1">
				{!! Form::open(['method' => 'DELETE', 'action' => ['MasterController@destroy', $diningRoom->id]]) !!}
    				<button class ="no-button glyphicon glyphicon-remove" type="submit"></button>
				{!! Form::close() !!}
			</div>

			<div class="col-md-4">
				
			</div>
		</div>
	@empty
		{{-- empty expr --}}
	@endforelse
@stop