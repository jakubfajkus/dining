@extends('master/master_master')

@section('content')
	{!! Form::open(['action' => $action , 'method' => $method]) !!}
		<div class="form-group {{ ($errors->has('name'))? 'has-error' : '' }}">
			{!! Form::label('name' , 'Název jídelny')!!}
			{!! Form::text('name' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('name' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('city'))? 'has-error' : '' }}">
			{!! Form::label('city' , 'Město')!!}
			{!! Form::text('city' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('city' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('zip'))? 'has-error' : '' }}">
			{!! Form::label('zip' , 'PSČ')!!}
			{!! Form::text('zip' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('zip' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('street'))? 'has-error' : '' }}">
			{!! Form::label('street' , 'Ulice')!!}
			{!! Form::text('street' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('street' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('email'))? 'has-error' : '' }}">
			{!! Form::label('email' , 'Kontaktní email')!!}
			{!! Form::text('email' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('email' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('phone'))? 'has-error' : '' }}">
			{!! Form::label('phone' , 'Telefon')!!}
			{!! Form::text('phone' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('phone' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('host'))? 'has-error' : '' }}">
			{!! Form::label('host' , 'Host')!!}
			{!! Form::text('host' , 'localhost' ,['class' => 'form-control']) !!}
			{!! $errors->first('host' , '<span class="help-block"> :message </span>') !!}
		</div>

		{!! Form::submit('Uložit' ,  ['class' => 'btn btn-primary']) !!}
	{!! Form::close() !!}
@endsection