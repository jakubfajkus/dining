@extends('user/user_master')

@section('content')
	@forelse ($days as $day)
		<div class="col-md-2">
			{{ $day->date }}
		</div>
		<div class="col-md-2">
			{{ $day->nameOfDay }}
		</div>
		<div class="col-md-8">
			@forelse ($day->dayOptions as $dayOption)
				<div class="row">
					<div class="col-md-3">
						{{ $dayOption->mealType->name }}
					</div>
					<div class="col-md-3">
						{!! link_to_action('UserMealController@show' , $dayOption->meal->name ,['id' => $dayOption->meal_id] ) !!}
					</div>					
					<div class="col-md-6">
						@if ($dayOption->ordered)
			    			{!! Form::open(['method' => 'DELETE', 'action' => ['UserOrderController@destroy',$dayOption->orders[0]->id ]]) !!}
								<input name="user_id" type="hidden" value="{{ $userId }}">
								<input name="day_option_id" type="hidden" value="{{ $dayOption->id }}">
			    				<button class ="btn" type="submit">Odhlásit</button>
							{!! Form::close() !!}
						@else
			    			{!! Form::open(['method' => 'POST', 'action' => ['UserOrderController@store', 'name'=>'jmeno']]) !!}
								<input name="user_id" type="hidden" value="{{ $userId }}">
								<input name="day_option_id" type="hidden" value={{ $dayOption->id }}>
			    				<button class ="btn btn-primary" type="submit">Objednat</button>
							{!! Form::close() !!}
						@endif
					</div>
				</div>				
		@empty
			<div class="no-day-option">
				<p>Žádná jídla na tento den</p>
			</div>
		@endforelse
	</div>
	@empty
		<p>Jídelna nevypsala žádná jídla</p>
	@endforelse
@endsection