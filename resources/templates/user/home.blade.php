@extends('user/user_master')

@section('title')
	{{ 'Domovská stránka' }}
@stop

@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Jste přihlášen jako</h4>
		</div>
		<div class="panel-body">
			{{ $user->name }}
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Váš email</h4>
		</div>
		<div class="panel-body">
			{{ $user->email }}
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Váš kredit</h4>
		</div>
		<div class="panel-body">
			{{ $user->credit }} Kč
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Jste z jídelny číslo</h4>
		</div>
		<div class="panel-body">
			{{ $diningRoomId }}
		</div>
	</div>

@stop