@extends('user/user_master')

@section('title')
	{{ 'Jidla' }}
@stop


@section('content')
	<h1>Seznam všech jídel v jídelně</h1>
	
	@forelse ($meals as $meal)
		<div>
			<h5>{!! link_to_route('user.meal.show' ,$meal->name , $meal->id ) !!}</h5>	
			<p>Cena: {{ $meal->price }} ,-Kč</p>		
			<p>{{ $meal->recipe }}</p>
			<p>{{ $meal->comment }}</p>
		</div>
	@empty
		{{ 'Nemáte žádná jídla' }}
	@endforelse
@stop