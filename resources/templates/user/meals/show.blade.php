@extends('user/user_master')

@section('title')
	{{ 'Detail jídla: ' . $meal->name }}
@stop

@section('content')


	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Název</h4>
		</div>
		<div class="panel-body">
			{{ $meal->name }}
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Kategorie</h4>
		</div>
		<div class="panel-body">
			@forelse ($mealTypes as $mealType)
				<p>{{ $mealType->name }}</p>	
			@empty
				<p>Jídlo není v žádných kategoriích</p>
			@endforelse
		</div>
	</div>
	
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Cena</h4>
		</div>
		<div class="panel-body">
			{{ $meal->price }} Kč
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Recept</h4>
		</div>
		<div class="panel-body">
			{{ $meal->recipe }}
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Komentář</h4>
		</div>
		<div class="panel-body">
			{{ $meal->comment }}
		</div>
	</div>
@stop