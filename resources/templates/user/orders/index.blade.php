@extends('user/user_master')

@section('content')
	@foreach ($days as $day)
		<div class="col-md-2">
			{{ $day->date }}
		</div>
		<div class="col-md-2">
			{{ $day->nameOfDay }}
		</div>
		<div class="col-md-8">
			@foreach ($day->dayOptions as $dayOption)
				<div class="line row">
					<div class="col-md-3">
						{{ $dayOption->mealType->name }}
					</div>
					<div class="col-md-3">
						{!! link_to_action('UserMealController@show' , $dayOption->meal->name ,['id' => $dayOption->meal_id] ) !!}
					</div>					
					<div class="col-md-6">
						@if ($dayOption->ordered)
							<button class ="btn disabled">Objednáno</button>
						@else
			    			<button class ="btn btn-primary disabled">Neobjednáno</button>
						@endif
					</div>
				</div>				
		@endforeach
	</div>
	@endforeach
@endsection