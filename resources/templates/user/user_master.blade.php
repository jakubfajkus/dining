@extends('master')

@section('navigation')
	<li>{!! link_to('/' , 'Hlavní stránka') !!}</li>
	<li>{!! link_to_route('user' , 'Domovská stránka') !!}</li>
	<li>{!! link_to_route('user.dayOption.index' , 'Přihlásit jídlo') !!}</li>
	<li>{!! link_to_route('user.meal.index' , 'Prohlížet jídla v jídelně') !!}</li>
	<li>{!! link_to_route('user.order.index' , 'Zobrazit objednávky') !!}</li>
	<li>{!! link_to('auth/logout' , 'Odhlásit') !!}</li>
@endsection