<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>
			@yield('title')
		</title>
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/styles/main.css">

		<script type="text/javascript" charset="utf-8" src="/js/jquery-2.1.3.js"></script>
		<script type="text/javascript" charset="utf-8" src="/js/bootstrap.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="/js/scripts.js"></script>
		@yield('scripts')

	</head>
	<body>
		<div class="container">
			<div class="header col-md-3">
				<ul class="nav nav-pills nav-stacked">
					@yield('navigation')
				</ul>
			</div>

			<div class="content col-md-9">
				<div class="unit">
					<div class="errors">
						@if ($errors->first('can_not_delete') )
							<div class="alert alert-danger" role="alert">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Chyba:</span>
								{!! $errors->first('can_not_delete') !!}
							</div>
						@endif

						@if ($errors->first('can_not_create') )
							<div class="alert alert-danger" role="alert">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Chyba:</span>
								{!! $errors->first('can_not_create') !!}
							</div>
						@endif
						@yield('errors')
					</div>
					@yield('content')
				</div>
			</div>
		</div>
</body>
</html>
