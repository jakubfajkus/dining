@extends('admin/admin_master')

@section('title')
	{{ 'Transakce' }}
@stop

@section('content')
	{!! link_to_route('admin.transaction.create' , 'Přidat novou transakci' , null , ['class' => 'btn btn-primary'])  !!}

	@include('subs.filter')

	@forelse ($transactions as $transaction)
		<div class="filtered-line row">
			<div class="col-md-9">
				<div class="details">
					<p>{!! link_to_route('admin.user.show' ,  $transaction->user->email ,$transaction->user->id ) !!}</p>
					<p>{{ $transaction->transaction_amount . ',-Kč' }}</p>
					<p>{{ $transaction->transactionType->name }}</p>
				</div>
			</div>					
			<div class="row col-md-3">					
				<a href={!! action('TransactionController@edit' , $transaction->id) !!}><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>
				{!! Form::open(['method' => 'DELETE', 'action' => ['TransactionController@destroy', $transaction->id]]) !!}
    				<button class ="no-button glyphicon glyphicon-remove" type="submit"></button>
				{!! Form::close() !!}
			</div>
		</div>
	@empty
		{{ 'Nemáte žádné transakce' }}
	@endforelse
@stop