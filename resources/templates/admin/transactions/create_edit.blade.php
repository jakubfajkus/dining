@extends('admin/admin_master')

@section('title')
	{{ 'Přidat novou transakci' }}
@stop

@section('content')
	{!! Form::model($transaction ,  array('action' => array($action , $transaction->id) , 'method' => $method)) !!}
		<div class="form-group {{ ($errors->has('user_id'))? 'has-error' : '' }}">
			{!! Form::label('user_id' , 'Email uživatele')!!}
			{!! Form::select('user_id' , $users , null , ['class' => 'form-control']) !!}
			{!! $errors->first('user_id' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('transaction_type_id'))? 'has-error' : '' }}">
			{!! Form::label('transaction_type_id' , 'Typ transakce')!!}
			{!! Form::select('transaction_type_id' , $transactionTypes , null ,['class' => 'form-control']) !!}
			{!! $errors->first('transaction_type_id' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('transaction_amount'))? 'has-error' : '' }}">
			{!! Form::label('transaction_amount' , 'Částka')!!}
			{!! Form::input('number','transaction_amount' , ($transaction->transaction_amount)? $transaction->transaction_amount : 200 ,['class' => 'form-control']) !!}
			{!! $errors->first('transaction_amount' , '<span class="help-block"> :message </span>') !!}
		</div>
		
		<div class="form-group {{ ($errors->has('comment'))? 'has-error' : '' }}">
			{!! Form::label('comment' , 'Komentář')!!}
			{!! Form::text('comment' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('comment' , '<span class="help-block"> :message </span>') !!}
		</div>
			{!! Form::submit('Uložit' , ['class' => 'btn btn-primary']) !!}	
	{!! Form::close() !!}

@stop