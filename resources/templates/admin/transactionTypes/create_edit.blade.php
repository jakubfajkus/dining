@extends('admin/admin_master')

@section('title')
	{{ 'Přidat nový typ transakce' }}
@stop

@section('content')

	{!! Form::model($transactionType ,  array('action' => array($action , $transactionType->id) , 'method' => $method)) !!}
		<div class="form-group {{ ($errors->has('name'))? 'has-error' : '' }}">
			{!! Form::label('name' , 'Název')!!}
			{!! Form::text('name' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('name' , '<span class="help-block"> :message </span>') !!}
		</div>
			{!! Form::submit('Uložit' , ['class' => 'btn btn-primary']) !!}	
				
	{!! Form::close() !!}

@stop