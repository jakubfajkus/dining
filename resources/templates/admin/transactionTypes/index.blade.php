@extends('admin/admin_master')

@section('title')
	{{ 'Typy transakcí' }}
@stop

@section('content')
	{!! link_to_route('admin.transactionType.create' , 'Přidat nový typ transakce' , null , ['class' => 'btn btn-primary'])  !!}
	
	@include('subs.filter')

	@forelse ($transactionTypes as $transactionType)
		<div class="filtered-line row">
			<div class="col-md-9">
				<p> {{  $transactionType->name }}</p>
			</div>					
			<div class="row col-md-3">					
				<a href={!! action('TransactionTypeController@edit' , $transactionType->id) !!}>
				<span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>
				{!! Form::open(['method' => 'DELETE', 'action' => ['TransactionTypeController@destroy', $transactionType->id]]) !!}
    				<button class ="no-button glyphicon glyphicon-remove" type="submit"></button>
				{!! Form::close() !!}
			</div>
		</div>
	@empty
		{{ 'Nemáte žádné typy transakcí' }}
	@endforelse
@stop