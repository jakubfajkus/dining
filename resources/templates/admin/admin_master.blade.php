@extends('master')

@section('navigation')
	<li>{!! link_to('/' , 'Hlavní stránka') !!}</li>
	<li>{!! link_to_route('admin' , 'Domovská stránka') !!}</li>
	<li>{!! link_to_route('admin.meal.index' , 'Jídla') !!}</li>
	<li>{!! link_to_route('admin.mealType.index' , 'Typy jídel') !!}<li>
	<li>{!! link_to_route('admin.user.index' , 'Strávníci') !!}</li>
	<li>{!! link_to_route('admin.dayOption.index' , 'Vypsaná jídla na dny') !!}</li>
	<li>{!! link_to_route('admin.dayOption.create' , 'Vypsat jídlo na den') !!}</li>
	<li>{!! link_to_route('admin.transaction.index' , 'Transakce') !!}</li>
	<li>{!! link_to_route('admin.transactionType.index' , 'Typy transakcí') !!}</li>
	<li>{!! link_to_route('admin.order.index' , 'Objednávky') !!}</li>
	<li>{!! link_to('auth/logout' , 'Odhlásit') !!}</li>
@endsection