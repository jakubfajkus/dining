@extends('admin/admin_master')

@section('title')
	{{ 'Dnešní objednávky' }}
@stop

@section('content')
	{!! link_to_route('admin.orderHistory.index' , 'Všechny objednávky' ,  null , ['class' => 'btn btn-primary'] )  !!}
	
	<div class="settings form-inline row">
		<div class="col-md-3 form-group">
			<label for="meal-types">Vyberte typ jídla:</label>
			<div class="form-group meal-types-select">
				{!! Form::select('mealType' , $mealTypes , null , ['class' => 'form-control'] ) !!}
			</div>
		</div>

		<div class="col-md-4 from-group">
			<label for="user">Uživatel(ID , email nebo jméno)</label>
			<input type="text" id='user' class="form-control">
		</div>

		<div class="col-md-5"> 
			<button id="reset" class="btn btn-primary">Resetovat</button>
		</div>
	</div>

	@forelse ($days as $day)
		@forelse ($day->orders as $order)
			<div class="filtered-line row">
				<div class="col-md-1 user-id">
					<p>{{ $order->user->id }}</p>
				</div>

				<div class="col-md-3 user-email">
					<p>{{ $order->user->email }}</p>
				</div>

				<div class="col-md-2 user-name">
					<p>{{ $order->user->name }}</p>
				</div>

				<div class="col-md-2 meal-type-name">
					<p>{{ $order->dayOption->mealType->name }}</p>
				</div>

				<div class="col-md-3 meal-name">
					<p>{{ $order->dayOption->meal->name }}</p>
				</div>

				<div class="col-md-1">
					@if($order->picked)
						<button class = "btn disabled" >Vyzvednuto</button>
					@else
						{!! Form::open(['method' => 'POST', 'action' => ['AdminOrderController@orderPicked']]) !!}
							<input type="hidden" name="id" value="{{ $order->id }}">
							<button class = "btn btn-primary" type="submit">Vyzvednuto</button>
						{!! Form::close() !!}
					@endif
				</div>
			</div>
		@empty
		@endforelse
	@empty
		<p>Nebyly nalezeny žádné dny s vypsanými jídly</p>
	@endforelse
@stop