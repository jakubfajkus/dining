@extends('admin/admin_master')

@section('content')

	@include('subs.filter')

	@foreach ($orders as $order)
		<div class="row filtered-line">
			<div class="col-md-2">
			{{ $order->dayOption->day->date }}
			</div>

			<div class="col-md-2">
				{{ $order->dayOption->day->nameOfDay }}
			</div>
			
			<div class="col-md-2">
				{{ $order->dayOption->mealType->name }}
			</div>

			<div class="col-md-2">
				{{ $order->dayOption->meal->name }}
			</div>

			<div class="col-md-2">
				{{ $order->user->id }}
			</div>

			<div class="col-md-2">
				{{ $order->user->email }}
			</div>
		</div>
	@endforeach
@endsection