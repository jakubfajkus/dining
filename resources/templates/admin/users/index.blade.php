@extends('admin/admin_master')

@section('title')
	{{ 'Strávníci' }}
@stop

@section('content')
	{!! link_to_route('admin.user.create' , 'Přidat nového strávníka' , null , ['class' => 'btn btn-primary'] ) !!}

	@include('subs.filter')

	@forelse ($users as $user)
		<div class="filtered-line row">
			<div class="col-md-9">
				<h5>{!! link_to_route('admin.user.show' , $user->name, array($user->id)) !!}</h5>
			</div>					
			<div class="col-md-3">					
				<a href={!! action('UserController@edit' , $user->id) !!}><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>

				{!! Form::open(['method' => 'DELETE', 'action' => ['UserController@destroy', $user->id]]) !!}
    				<button class ="no-button glyphicon glyphicon-remove" type="submit"></button>
				{!! Form::close() !!}
			</div>
		</div>
	@empty
		{{ 'Nemáte žádné strávníky' }}
	@endforelse
@stop