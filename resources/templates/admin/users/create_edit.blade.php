@extends('admin/admin_master')

@section('title')
	{{ 'Přidat nového strávníka' }}
@stop

@section('content')

	{!! Form::model($user ,  array('action' => array($action , $user->id) , 'method' => $method)) !!}
	<div class="form-group {{ ($errors->has('name'))? 'has-error' : '' }}">
		{!! Form::label('name' , 'Jméno') !!}
		{!! Form::text('name' , null ,['class' => 'form-control']) !!}
		{!! $errors->first('name' , '<span class="help-block"> :message </span>') !!}
	</div>

	<div class="form-group {{ ($errors->has('email'))? 'has-error' : '' }}">
		{!! Form::label('email' , 'Email') !!}
		{!! Form::text('email' , null ,['class' => 'form-control']) !!}
		{!! $errors->first('email' , '<span class="help-block"> :message </span>') !!}
	</div>

	<div class="form-group {{ ($errors->has('password'))? 'has-error' : '' }}">
		{!! Form::label('password' , 'Heslo') !!}
		{!! Form::text('password' , null ,['class' => 'form-control']) !!}
		{!! $errors->first('password' , '<span class="help-block"> :message </span>') !!}
	</div>

	<div class="form-group {{ ($errors->has('city'))? 'has-error' : '' }}">
		{!! Form::label('city' , 'Město') !!}
		{!! Form::text('city' , null ,['class' => 'form-control']) !!}
		{!! $errors->first('city' , '<span class="help-block"> :message </span>') !!}
	</div>
		
	<div class="form-group {{ ($errors->has('zip'))? 'has-error' : '' }}">
		{!! Form::label('zip' , 'PSČ') !!}
		{!! Form::text('zip' , null ,['class' => 'form-control']) !!}
		{!! $errors->first('zip' , '<span class="help-block"> :message </span>') !!}
	</div>

	<div class="form-group {{ ($errors->has('street'))? 'has-error' : '' }}">
		{!! Form::label('street' , 'Ulice a číslo popisné') !!}
		{!! Form::text('street' , null ,['class' => 'form-control']) !!}
		{!! $errors->first('street' , '<span class="help-block"> :message </span>') !!}
	</div>

	<div class="form-group">
		{!! Form::label('admin' , 'Administrátor') !!}
		{!! Form::checkbox('admin' ,'admin' , $isAdmin ,  ['class' => 'form-controll'])  !!} ano
	</div>

	<div class="form-group {{ ($errors->has('comment'))? 'has-error' : '' }}">
		{!! Form::label('comment' , 'Komentář') !!}
		{!! Form::textarea('comment' , null ,['class' => 'form-control']) !!}
		{!! $errors->first('comment' , '<span class="help-block"> :message </span>') !!}
	</div>

		{!! Form::submit('Uložit' , ['class' => 'btn btn-primary']) !!}	
	</div>

	{!! Form::close() !!}

@stop