@extends('admin/admin_master')

@section('title')
	{{ 'Detail ' . $user->name }}
@stop

@section('content')
	
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>ID</h4>
		</div>
		<div class="panel-body">
			{{ $user->id }}
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Jméno</h4>
		</div>
		<div class="panel-body">
			{{ $user->name }}
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Email</h4>
		</div>
		<div class="panel-body">
			{{ $user->email }}
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Město</h4>
		</div>
		<div class="panel-body">
			{{ $user->city }}
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>PSČ</h4>
		</div>
		<div class="panel-body">
			{{ $user->zip }}
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Ulice</h4>
		</div>
		<div class="panel-body">
			{{ $user->street }}
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Komentář</h4>
		</div>
		<div class="panel-body">
			{{ $user->comment }}
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Kredit</h4>
		</div>
		<div class="panel-body">
			{{ $user->credit }} Kč
		</div>
	</div>
@stop