@extends('admin/admin_master')

@section('title')
	{{ 'Historie vypsaných jídel' }}
@stop

@section('content')
	
	@include('subs.filter')
	
	@forelse ($days as $day)
	<div class="filtered-line">
		<div class="col-md-2">
			{{ $day->date }}
		</div>
		<div class="col-md-1">
			{{ $day->nameOfDay }}
		</div>
		<div class="col-md-9">
			@forelse ($day->dayOptions as $dayOption)
				<div class="row">
					<div class="col-md-2">
						{{ $dayOption->mealType->name }}
					</div>	

					<div class="col-md-6">
						{!! link_to_action('MealController@show' , $dayOption->meal->name ,['id' => $dayOption->meal_id] ) !!}
					</div>		

					<div class="col-md-2">
						{{ $dayOption->comment }}
					</div>

					<div class="col-md-1">
						{{ $dayOption->countOfOrders }}
					</div>

					<div class="col-md-1">	
					<!--  -->
					</div>
				</div>
					
			@empty
				<div class="no-day-option">
					<p>Žádná jídla na tento den</p>
				</div>
			@endforelse
		</div>
	@empty
		<p>Nebyly vypsány žádné dny</p>
	@endforelse
	</div>
		
@stop