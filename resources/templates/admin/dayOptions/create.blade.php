@extends('admin/admin_master')

@section('title')
	{{ 'Přidat nové jídlo na den' }}
@stop

@section('content')
	{!! Form::model($dayOption ,  array('action' => array($action , $dayOption->id) , 'method' => $method)) !!}
		<div class="form-group {{ ($errors->has('date'))? 'has-error' : '' }}">
			{!! Form::label('date' ,  'Datum') !!}
			{!! Form::input('date' , 'date' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('date' , '<span class="help-block"> :message </span>') !!}
		</div>
		<div class="form-group {{ ($errors->has('meal_id'))? 'has-error' : '' }}">
			{!! Form::label('meal_id' , 'Jídlo')!!}
			{!! Form::select('meal_id' , $meals , null , ['class' => 'form-control']) !!}
			{!! $errors->first('meal_id' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('meal_type_id'))? 'has-error' : '' }}">
			{!! Form::label('meal_type_id' , 'Doba, na kterou jídlo přiřadit')!!}
			{!! Form::select('meal_type_id' , $mealTypes , null ,['class' => 'form-control']) !!}
			{!! $errors->first('meal_type_id' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('name'))? 'has-error' : '' }}">
			{!! Form::label('comment' , 'Komentář')!!}
			{!! Form::text('comment' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('comment' , '<span class="help-block"> :message </span>') !!}
		</div>
			{!! Form::submit('Uložit' , ['class' => 'btn btn-primary']) !!}	
				
	{!! Form::close() !!}

@stop