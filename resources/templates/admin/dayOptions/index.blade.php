@extends('admin/admin_master')

@section('title')
	{{ 'Přidání jídel na dny' }}
@stop

@section('content')
	<div>
		{!! link_to_route('admin.dayOption.create' , 'Vypsat jídlo na den' , null , ['class' => 'btn btn-primary']) !!}
		{!! link_to_route('admin.dayOptionHistory.index' , 'Historie vypsaných jídel' , null , ['class' => 'btn btn-primary']) !!}
	</div>

	@include('subs.filter')

	@forelse ($days as $day)
		<div class="filtered-line">
			<div class="col-md-2">
				{{ $day->date }}
			</div>
			<div class="col-md-1">
				{{ $day->nameOfDay }}
			</div>
			<div class="col-md-9">
				@forelse ($day->dayOptions as $dayOption)
					<div class="row">
						<div class="col-md-2">
							{{ $dayOption->mealType->name }}
						</div>	

						<div class="col-md-4">
							{!! link_to_action('MealController@show' , $dayOption->meal->name ,['id' => $dayOption->meal_id] ) !!}
						</div>		

						<div class="col-md-2">
							{{ $dayOption->comment }}
						</div>

						<div class="col-md-2">
							{{ $dayOption->countOfOrders }}
						</div>

						<div class="col-md-1">
							<a href={!! action('DayOptionController@edit' , $dayOption->id) !!} class="glyphicon glyphicon-cog" aria-hidden="true">
							</a>
						</div>

						<div class="col-md-1">
							{!! Form::open(['method' => 'DELETE', 'action' => ['DayOptionController@destroy', $dayOption->id ]]) !!}
							<button class ="no-button glyphicon glyphicon-remove" type="submit"></button>
						{!! Form::close() !!}		
						</div>
					</div>
						
				@empty
					<div class="no-day-option">
						<p>Žádná jídla na tento den</p>
					</div>
				@endforelse
			</div>
		</div>
		@empty
			<p>Nebyly vypsány žádné dny</p>
	@endforelse
		
@stop