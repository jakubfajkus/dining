@extends('admin/admin_master')

@section('title')
	{{ 'Detail ' . $meal->name }}
@stop

@section('scripts')
	<script type="text/javascript" charset="utf-8" src="/js/confirm_destroy.js"></script>
@stop

@section('content')

	<div class="button-group">
		{!! link_to_route('admin.meal.edit' , 'Editovat' , $meal->id , ['class' => 'btn btn-primary']) !!}

		{!! Form::open(['method' => 'DELETE', 'action' => ['MealController@destroy', $meal->id]]) !!}
    		<button class="btn btn-primary" type="submit">Odstranit</button>
		{!! Form::close() !!}
		
	</div>
	
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Název</h4>
		</div>
		<div class="panel-body">
			{{ $meal->name }}
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Kategorie</h4>
		</div>
		<div class="panel-body">
			@forelse ($mealTypes as $mealType)
				<p>{{ $mealType->name }}</p>	
			@empty
				<p>Jídlo není v žádných kategoriích</p>
			@endforelse
		</div>
	</div>
	
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Cena</h4>
		</div>
		<div class="panel-body">
			{{ $meal->price }} Kč
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Recept</h4>
		</div>
		<div class="panel-body">
			{{ $meal->recipe }}
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Komentář</h4>
		</div>
		<div class="panel-body">
			{{ $meal->comment }}
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Počet objednávek celkem</h4>
		</div>
		<div class="panel-body">
			{{ $meal->countOfOrders }}
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Jídlo bylo vypsáno celkem</h4>
		</div>
		<div class="panel-body">
			{{ $meal->countOfDayOptions }}x
		</div>
	</div>

@stop