@extends('admin/admin_master')

@section('title')
	{{ 'Upravit nebo přidat jídlo' }}
@stop

@section('content')

	{!! Form::model($meal ,  array('action' => array($action , $meal->id) , 'method' => $method)) !!}
		<div class="form-group {{ ($errors->has('name'))? 'has-error' : '' }}">
			{!! Form::label('name' , 'Název')!!}
			{!! Form::text('name' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('name' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('price'))? 'has-error' : '' }}">
			{!! Form::label('price' , 'Cena jídla')!!}
			{!! Form::input( 'number' ,'price' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('price' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('mealType'))? 'has-error' : '' }}">
			<ul>
				@forelse ($mealTypes as $mealType)
					<li>
						<div class="checkbox">
							{!! Form::checkbox('mealType[]' ,$mealType->name , $mealType->checked ) !!}
						</div>
						{{ $mealType->name }}	
					</li>
				@empty
					<h1>Žádné typy jídel, přidejte nějaké prosím</h1>
				@endforelse
			</ul>
			{!! $errors->first('mealType' , '<span class="help-block"> :message </span>') !!}
		</div>
		

		<div class="form-group {{ ($errors->has('name'))? 'has-error' : '' }}">
			{!! Form::label('recipe' , 'Recept')!!}
			{!! Form::textarea('recipe' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('recipe' , '<span class="help-block"> :message </span>') !!}
		</div>

		<div class="form-group {{ ($errors->has('name'))? 'has-error' : '' }}">
			{!! Form::label('comment' , 'Komentář')!!}
			{!! Form::text('comment' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('comment' , '<span class="help-block"> :message </span>') !!}
		</div>
			{!! Form::submit('Uložit' , ['class' => 'btn btn-primary']) !!}	
				
	{!! Form::close() !!}

@stop