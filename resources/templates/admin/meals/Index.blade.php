@extends('admin/admin_master')

@section('title')
	{{ 'Jidla' }}
@stop

@section('content')
	{!! link_to_route('admin.meal.create' , 'Přidat nové jídlo' , null , ['class' => 'btn btn-primary']) !!}

	@include('subs.filter')

	@forelse ($meals as $meal)
		<div class="filtered-line row">
			<div class="col-md-9">
				<h5>{!! link_to_route('admin.meal.show' , $meal->name, array($meal->id)) !!}</h5>
			</div>					
			<div class="meal-row col-md-3">					
				<a href={!! action('MealController@edit' , $meal->id) !!}><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>
				
				{!! Form::open(['method' => 'DELETE', 'action' => ['MealController@destroy', $meal->id]]) !!}
    				<button class ="no-button glyphicon glyphicon-remove" type="submit"></button>
				{!! Form::close() !!}
			</div>
		</div>
	@empty
		{{ 'Nemáte žádná jídla' }}
	@endforelse
@stop