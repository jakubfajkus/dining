@extends('admin/admin_master')

@section('title')
	{{ 'Typy jídel' }}
@stop

@section('content')
	{!! link_to_route('admin.mealType.create' , 'Přidat nový' , null , ['class' => 'btn btn-primary'] ) !!}

	@include('subs.filter')

	@forelse ($mealTypes as $mealType)
		<div class="filtered-line row">
			<div class="col-md-9">
				<h5>{!! link_to_route('admin.mealType.show' , $mealType->name, array($mealType->id)) !!}</h5>
			</div>					
			<div class="meal-type-row col-md-3">					
				<a href={!! action('MealTypeController@edit' , $mealType->id) !!}><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>

				{!! Form::open(['method' => 'DELETE', 'action' => ['MealTypeController@destroy', $mealType->id]]) !!}
    				<button class ="no-button glyphicon glyphicon-remove" type="submit"></button>
				{!! Form::close() !!}
			</div>
		</div>
	@empty
		{{ 'Nemáte žádné typy jídel' }}
	@endforelse
@stop