@extends('admin/admin_master')

@section('title')
	{{ 'Detail ' . $mealType->name }}
@stop

@section('content')
	
	<div class="button-group">
		<a class="btn btn-primary"  href={!! action('MealTypeController@edit' , $mealType->id) !!}> Editovat </a>
		{!! Form::open(['method' => 'DELETE', 'action' => ['MealTypeController@destroy', $mealType->id]]) !!}
			<button class ="btn btn-primary" type="submit">Odstranit</button>
		{!! Form::close() !!}
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>Jméno</h4>
		</div>
		<div class="panel-body">
			{{ $mealType->name }}
		</div>
	</div>
@stop