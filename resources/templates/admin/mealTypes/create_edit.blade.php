@extends('admin/admin_master')

@section('title')
	{{ 'Přidat nový typ jídla' }}
@stop

@section('content')

	{!! Form::model($mealType ,  array('action' => array($action , $mealType->id) , 'method' => $method)) !!}
		<div class="form-group {{ ($errors->has('name'))? 'has-error' : '' }}">
			{!! Form::label('name' , 'Jméno')!!}
			{!! Form::text('name' , null ,['class' => 'form-control']) !!}
			{!! $errors->first('name' , '<span class="help-block"> :message </span>') !!}
		</div>
			{!! Form::submit('Uložit' , ['class' => 'btn btn-primary']) !!}	
				
	{!! Form::close() !!}

@stop