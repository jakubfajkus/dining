@extends('master')

@section('title')
	{{ 'Jidelny' }}
@stop

@section('content')
	@if (!$loggedIn)
		<h1>Vítejte, můžete se přihlásit kliknutím na tlačítko.</h1>
		<a href="{!! url('auth/login') !!}"><button class="btn btn-primary">Přihlásit</button></a>
	@else
		<h1>Vítejte</h1>
		@if($isAdmin)
			<a href="{!! url('admin') !!}"><button class="btn btn-primary">Administrace</button></a>
			<a href="{!! url('user') !!}"><button class="btn btn-primary">Uživatelská sekce</button></a>
		@else
			<a href="{!! url('user') !!}"><button class="btn btn-primary">Do jídelny</button></a>
		@endif
	
		<a href="{!! url('auth/logout') !!}"><button class="btn btn-primary">Odhlásit</button></a>
	@endif

@stop