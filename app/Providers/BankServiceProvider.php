<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Bank\BankHandler;

class BankServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('bankHandler' , function($app)
		{
			return new \App\Bank\BankHandler;
		});	
	}

}
