<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
	
class Day extends Model
{	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'days';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['date' , 'comment'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * Varaibles included in the toArray method
	 * @var [type]
	 */
	protected $appends = ['nameOfDay'];

	public function dayOptions()
	{
		return $this->hasMany('App\DayOption');
	}

	public function meals()
	{
		return $this->belongsToMany('App\Meal' , 'day_options');
	}

	public function orders()
	{
		return $this->hasManyThrough('App\Order' , 'App\DayOption');
	}

	/**
	 * Formats the date to format dd.mm.yyyy
	 */
	public function formatToCzechDate()
	{
		$date = new \DateTime($this->date);
		$this->date = $date->format('d.m.Y');
	}

	public function setCzechNameOfDay()
	{
		$date = new \DateTime($this->date);
		$numberOfDay = $date->format('N');

		switch ($numberOfDay) 
		{
			case 1:
				$this->nameOfDay = "Pondělí";
				break;
		
			case 2:
				$this->nameOfDay = "Úterý";
				break;

			case 3:
				$this->nameOfDay = "Středa";
				break;

			case 4:
				$this->nameOfDay = "Čtvrtek";
				break;
		
			case 5:
				$this->nameOfDay = "Pátek";
				break;

			case 6:
				$this->nameOfDay = "Sobota";
				break;

			case 7:
				$this->nameOfDay = "Neděle";
				break;

			default:
				$this->nameOfDay = "chyba";
				break;
		}
	}
}