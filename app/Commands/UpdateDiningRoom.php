<?php namespace App\Commands;

use App\Commands\Command;
use Illuminate\Support\Facades\DB;

use Illuminate\Contracts\Bus\SelfHandling;

class UpdateDiningRoom extends Command implements SelfHandling 
{
	protected $id; 
	protected $name; 
	protected $city; 
	protected $zip;  
	protected $street;
	protected $email;
	protected $telephone;
	protected $host;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct( $id ,$name, $city, $zip, $street, $email, $telephone, $host )
	{
		$this->id 		 = $id; 
		$this->name 	 = $name; 
		$this->city 	 = $city; 
		$this->zip 		 = $zip;  
		$this->street 	 = $street; 
		$this->email 	 = $email;
		$this->telephone = $telephone;
		$this->host 	 = $host;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		DB::connection('master')->table('dining_rooms')->where('id' , $this->id)->update([
			'id' 		=> $this->id,
			'name' 		=> $this->name,
			'city' 		=> $this->city,
			'zip' 		=> $this->zip ,
			'street'	=> $this->street,
			'email' 	=> $this->email,
			'telephone' => $this->telephone,
			'host' 		=> $this->host
		]);
	}

}