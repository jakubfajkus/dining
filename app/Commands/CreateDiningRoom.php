<?php namespace App\Commands;

use App\Commands\Command;
use App\Helpers\DatabaseHelper;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class CreateDiningRoom extends Command implements SelfHandling 
{
	protected $name; 
	protected $city; 
	protected $zip;  
	protected $street;
	protected $email;
	protected $phone;
	protected $host;
	protected $diningAdminPassword;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct( $name, $city, $zip, $street, $email, $phone, $host ,$diningAdminPassword )
	{
		$this->name 	= $name; 
		$this->city 	= $city; 
		$this->zip 		= $zip;  
		$this->street 	= $street; 
		$this->email 	= $email;
		$this->phone 	= $phone;
		$this->host 	= $host;
		$this->diningAdminPassword 	= $diningAdminPassword;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$databaseHelper = new DatabaseHelper();

		//create a DB
		$lastId = $databaseHelper->getLastDatabaseId();
		$databaseName = 'dining_' . ($lastId+1);
		$databaseHelper->createDatabase($databaseName);

		//increment last_dining_room_id in the DB
		$databaseHelper->setLastDatabaseId($lastId+1);

		//generate username
		$username = substr(md5(rand()) , 0 , 16);

		//generate password
		$password = substr(md5(rand()) , 0 , 16);

		//generate random hash for username
		$usernameHash = md5(rand());		

		//generate random hash for password
		$passwordHash = md5(rand());		

		//create a file for storing username
		Storage::put(config('database.diningRoomsInfoFolder') . '/' . $usernameHash , $username);

		//create a file for storing password
		Storage::put(config('database.diningRoomsInfoFolder') . '/' . $passwordHash , $password);
		
		//create an DB user
		$databaseHelper->createUser($username , $password , $databaseName , $this->host);

		//put record to the dining_maser.dining_rooms table
		$databaseHelper->addRecordToDiningMaster($this->name, $this->city, $this->zip, $this->street, $this->email, $this->phone, $this->host, $usernameHash, $passwordHash );
		
		//migrate database
		$databaseHelper->migrateDatabase($databaseName);

		//create first user
		$databaseHelper->createFirstUser($this->email, $this->diningAdminPassword, $this->city, $this->zip, $this->street, $this->name);
	}

}