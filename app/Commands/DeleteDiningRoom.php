<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;

class DeleteDiningRoom extends Command implements SelfHandling 
{
	protected $id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($id)
	{
		$this->id = $id;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$folder = Config::get('database.diningRoomsInfoFolder');

		// delete dining room database
		$dbName = 'dining_' . $this->id;
		DB::connection('master')->statement('DROP DATABASE ' . $dbName);

		// delete user
		$userResult = DB::connection('master')->table('dining_rooms')
											  ->select(['username' , 'password' , 'host'])
											  ->where('id', $this->id)
											  ->first();
		// real username is stored in file
		$username = Storage::get($folder . '/' . $userResult->username);
		DB::connection('master')->statement('DROP USER ' . $username . '@' . $userResult->host);
		
		// delete files with username and password
		Storage::delete($folder . '/' . $userResult->username);
		Storage::delete($folder . '/' . $userResult->password);

		// delete record in dining_master database
		DB::connection('master')->statement('DELETE FROM dining_rooms WHERE id=' . $this->id);
	}

}