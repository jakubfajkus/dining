<?php namespace App\Exceptions;

/**
* 
*/
class CanNotDeleteException extends \Exception
{
	protected $message = 'Can not delete record. Some other records depends on it.';
}