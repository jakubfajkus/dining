<?php namespace App\Exceptions;

/**
* 
*/
class CanNotCreateException extends \Exception
{
	protected $message = 'Can not create record.';
}