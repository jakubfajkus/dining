<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
	public static $PICKED_CODE = 1;
	public static $NON_PICKED_CODE = 0;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id' , 'day_option_id' , 'picked'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public static function create(array $values = array())
	{
		$user = $values['user'];

		$order = static::find(['user_id' => $user->id, 'day_option_id' => $values['day_option_id']]);

		if($order->isEmpty())
		{
			//order does not exist
			//create it
			
			parent::create(['user_id' => $user->id, 'day_option_id' => $values['day_option_id']]);	

			$priceOfMeal = DayOption::find($values['day_option_id'])->meal->price;
			$user->subtractCredit($priceOfMeal);
		}
	}

	public function delete()
	{
		//get price of the meal
		$priceOfMeal = DayOption::find($this->day_option_id)->meal->price;

		//find the user
		$user = User::find($this->user_id);

		//add credit to the user
		$user->addCredit($priceOfMeal);
		//delete order
		parent::delete();
	}

	public function setPicked()
	{
		$this->picked = self::$PICKED_CODE;
		$this->save();
	}

	public function setNonPicked()
	{
		$this->picked = self::$NON_PICKED_CODE;
		$this->save();
	}

	public function isPicked()
	{
		return $this->picked == self::$PICKED_CODE;
	}

	public function dayOption()
	{
		return $this->belongsTo('\App\DayOption');
	}

	public function user()
	{
		return $this->belongsTo('\App\User');
	}

}