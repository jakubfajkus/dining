<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CheckBank extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'bank';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check bank for all transactions.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$bankHandler = new \App\Bank\BankHandler($this->argument('diningRoomId'));
		$bankHandler->processAll();
	}

	public function getArguments()
	{
		return [['diningRoomId' , InputArgument::REQUIRED , 'ID of the dining room']];
	}
}
