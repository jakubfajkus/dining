<?php namespace App;

use App\Exceptions\CanNotDeleteException;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionType extends Model
{	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transaction_types';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	protected $dates = ['deleted_at'];

	public function delete()
	{
		if(! $this->transactions->isEmpty())
		{
			throw new CanNotDeleteException("Can not delete transaction type. Thee are some transactions of this type", 1);
		}
		else
		{
			parent::delete();
		}
	}

	public function transactions()
	{
		return $this->hasMany('App\Transaction');
	}
}