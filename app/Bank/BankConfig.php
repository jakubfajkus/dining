<?php namespace App\Bank;

/**
* 	
*/
class BankConfig
{
	/* Date, which serves as starting point for getting all bank transactions */
	protected $dateFrom;

	/* Date, which serves as ending point for getting all bank transactions */
	protected $dateTo;

	/* Prefabriced URL to the GET request to the api. 
	   It needs to be completed(replace TOKEN , DATE_FROM , DATE_TO widlcards)*/
	protected $apiUrl = null;
	
	/* Date format, which is required by the API */
	protected $dateFormat = 'Y-m-d';

    /* Data format, which is provided by API. It is used in this class */
	protected $dataFormatJSON = 'json';

    /* Data format, which is provided by API. It is not used in this class */
	protected $dataFormatXML = 'xml';

    /* Transactio types, which are balance possitive for bank account. 
       That are incomming payments.
    */
	protected $incomingBankTransferTypes = 
	[
		'Příjem převodem uvnitř banky',
		'Příjem',
		'Bezhotovostní příjem',
		'Převod mezi bankovními konty (příjem)',
		'Neidentifikovaný příjem na bankovní konto',
		'Vlastní příjem na bankovní konto'
	];

	public function __construct($apiUrl = null , $dateFrom = '2014-01-01' , $dateTo = '2040-01-01')
	{
		$this->dateFrom = \DateTime::createFromFormat($this->dateFormat , $dateFrom);
		$this->dateTo 	= \DateTime::createFromFormat($this->dateFormat , $dateTo);

        $this->apiUrl = $apiUrl;
	}

    /**
     * Gets the value of dateFrom.
     *
     * @return mixed
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Sets the value of dateFrom.
     *
     * @param mixed $dateFrom the date from
     *
     * @return self
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Gets the value of dateTo.
     *
     * @return mixed
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Sets the value of dateTo.
     *
     * @param mixed $dateTo the date to
     *
     * @return self
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Gets the value of token.
     *
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Sets the value of token.
     *
     * @param mixed $token the token
     *
     * @return self
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Gets the value of apiUrl.
     *
     * @return mixed
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * Sets the value of apiUrl.
     *
     * @param mixed $apiUrl the api url
     *
     * @return self
     */
    public function setApiUrl($apiUrl)
    {
        $this->apiUrl = $apiUrl;

        return $this;
    }

    /**
     * Gets the value of dateFormat.
     *
     * @return mixed
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * Sets the value of dateFormat.
     *
     * @param mixed $dateFormat the date format
     *
     * @return self
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = $dateFormat;

        return $this;
    }

    /**
     * Gets the value of dataFormatJSON.
     *
     * @return mixed
     */
    public function getDataFormatJSON()
    {
        return $this->dataFormatJSON;
    }

    /**
     * Sets the value of dataFormatJSON.
     *
     * @param mixed $dataFormatJSON the data format
     *
     * @return self
     */
    public function setDataFormatJSON($dataFormatJSON)
    {
        $this->dataFormatJSON = $dataFormatJSON;

        return $this;
    }

    /**
     * Gets the value of dataFormatXML.
     *
     * @return mixed
     */
    public function getDataFormatXML()
    {
        return $this->dataFormatXML;
    }

    /**
     * Sets the value of dataFormatXML.
     *
     * @param mixed $dataFormatXML the data format
     *
     * @return self
     */
    public function setDataFormatXML($dataFormatXML)
    {
        $this->dataFormatXML = $dataFormatXML;

        return $this;
    }

    /**
     * Gets the value of incomingBankTransferTypes.
     *
     * @return mixed
     */
    public function getIncomingBankTransferTypes()
    {
        return $this->incomingBankTransferTypes;
    }

    /**
     * Sets the value of incomingBankTransferTypes.
     *
     * @param mixed $incomingBankTransferTypes the incoming bank transfer types
     *
     * @return self
     */
    public function setIncomingBankTransferTypes($incomingBankTransferTypes)
    {
        $this->incomingBankTransferTypes = $incomingBankTransferTypes;

        return $this;
    }
}