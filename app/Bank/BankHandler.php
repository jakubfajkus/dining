<?php namespace App\Bank;

use App\Commands\CreateTransaction;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use App\User;
use App\TransactionType;
use App\Helpers\DBSwitcher;
/**
* 
*/
class BankHandler
{	
	protected $config;
	protected $diningRoomId;

	function __construct($diningRoomId = null) 
	{
		if (is_null($diningRoomId)) 
		{
			throw new \InvalidArgumentException("Dining room id required", 1);
		}

		$DBSwitcher = new DBSwitcher($diningRoomId , config('database.diningRoomsInfoFolder'));
		$DBSwitcher->switchDB();

		$info = DB::connection()->table('info')->select('*')->first();

		//if the a API data are not provided, throw exception
		if(is_null($info) || is_null($info->bank_api_date_from) || is_null($info->bank_api_date_to))
		{
			throw new \Exception("No dining room API data found", 1);
		}

		$this->config = new BankConfig($info->url , $info->bank_api_date_from , $info->bank_api_date_to);	
		$this->diningRoomId = $diningRoomId;

	}

	public function processAll()
	{
		$data = $this->process( $this->config->getDateFrom() , $this->config->getDateTo() );
		$parser = new Parser($data);
		$parser->parse();
		$transactions = $parser->getReceivedPayments();

		$this->createTransactions($transactions);
	}

	protected function process(\DateTime $from , \DateTime $to)
	{
		$url = $this->createURL();
		$data = file_get_contents($url);

		return json_decode($data);
	}

	protected function createURL()
	{
		$url = $this->config->getApiUrl();

		$url = str_replace('DATE_FROM'	, $this->config->getDateFrom()->format($this->config->getDateFormat() ) , $url);
		$url = str_replace('DATE_TO'	, $this->config->getDateTo()->format($this->config->getDateFormat() ) , $url);
		$url = str_replace('FORMAT'		, $this->config->getDataFormatJSON() , $url);

		return $url;
	}

	public function createTransactions($transactions)
	{
		foreach ($transactions as $transaction) 
		{
			//check, if is the transaction type supported
			if(! in_array($transaction->getType(), $this->config->getIncomingBankTransferTypes() ) )
			{
				continue;
			}

			//check, if the user exists
			$user = User::find( $transaction->getVariableSymbol() );
			if( is_null($user) )
			{
				//user does not exist
				//should log somewhere
				continue;
			}

			//check, if the transaction_type exists or create a new one			
			$transactionType =  TransactionType::firstOrCreate( ['name' => $transaction->getType()] );

			//create transaction
			Bus::dispatch( new CreateTransaction(
				$transaction->getVariableSymbol(), // user_id
				$transaction->getAmount(),
				$transactionType->id,
				$transaction->getTransactionId(),
				$transaction->getComment(),
				$transaction->getAccount(),
				$transaction->getBankCode(),
				$transaction->getBankName(),
				$transaction->getConstantSymbol(),
				$transaction->getVariableSymbol(),
				$transaction->getSpecificSymbol(),
				$transaction->getUserIdentification(),
				$transaction->getMessage(),
				$transaction->getType(),
				$transaction->getComment()
			) );
		}
	}
}