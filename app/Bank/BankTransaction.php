<?php namespace App\Bank;

/**
* 
*/
class BankTransaction
{
	protected $transactionId;
	protected $date;
	protected $amount;
	protected $currency;
	protected $account;
	protected $bankCode;
	protected $bankName;
	protected $constantSymbol;
	protected $variableSymbol;
	protected $specificSymbol;
	protected $userIdentification;
	protected $message;
	protected $type;
	protected $comment;
	protected $instructionId;

	public static $bindings = 
	[
		22 	=>  'TransactionId',
		0	=>	'Date',
		1	=>	'Amount',
		14	=>	'Currency',
		2	=>	'Account',
		3	=>	'BankCode',
		12	=>	'BankName',
		4	=>	'ConstantSymbol',
		5	=>	'VariableSymbol',
		6	=>	'SpecificSymbol',
		7	=>	'UserIdentification',
		16	=>	'Message',
		8	=>	'Type',
		25	=>	'Comment',
		17	=>	'InstructionId'
	];
	
	function __construct()
	{
		
	}

    

    /**
     * Gets the value of transactionId.
     *
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Sets the value of transactionId.
     *
     * @param mixed $transactionId the transaction id
     *
     * @return self
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Gets the value of date.
     *
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the value of date.
     *
     * @param mixed $date the date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Gets the value of amount.
     *
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Sets the value of amount.
     *
     * @param mixed $amount the amount
     *
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Gets the value of currency.
     *
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Sets the value of currency.
     *
     * @param mixed $currency the currency
     *
     * @return self
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Gets the value of account.
     *
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Sets the value of account.
     *
     * @param mixed $account the account
     *
     * @return self
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Gets the value of bankCode.
     *
     * @return mixed
     */
    public function getBankCode()
    {
        return $this->bankCode;
    }

    /**
     * Sets the value of bankCode.
     *
     * @param mixed $bankCode the bank code
     *
     * @return self
     */
    public function setBankCode($bankCode)
    {
        $this->bankCode = $bankCode;

        return $this;
    }

    /**
     * Gets the value of bankName.
     *
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * Sets the value of bankName.
     *
     * @param mixed $bankName the bank name
     *
     * @return self
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;

        return $this;
    }

    /**
     * Gets the value of constantSymbol.
     *
     * @return mixed
     */
    public function getConstantSymbol()
    {
        return $this->constantSymbol;
    }

    /**
     * Sets the value of constantSymbol.
     *
     * @param mixed $constantSymbol the constant symbol
     *
     * @return self
     */
    public function setConstantSymbol($constantSymbol)
    {
        $this->constantSymbol = $constantSymbol;

        return $this;
    }

    /**
     * Gets the value of variableSymbol.
     *
     * @return mixed
     */
    public function getVariableSymbol()
    {
        return $this->variableSymbol;
    }

    /**
     * Sets the value of variableSymbol.
     *
     * @param mixed $variableSymbol the variable symbol
     *
     * @return self
     */
    public function setVariableSymbol($variableSymbol)
    {
        $this->variableSymbol = $variableSymbol;

        return $this;
    }

    /**
     * Gets the value of specificSymbol.
     *
     * @return mixed
     */
    public function getSpecificSymbol()
    {
        return $this->specificSymbol;
    }

    /**
     * Sets the value of specificSymbol.
     *
     * @param mixed $specificSymbol the specific symbol
     *
     * @return self
     */
    public function setSpecificSymbol($specificSymbol)
    {
        $this->specificSymbol = $specificSymbol;

        return $this;
    }

    /**
     * Gets the value of userIdentification.
     *
     * @return mixed
     */
    public function getUserIdentification()
    {
        return $this->userIdentification;
    }

    /**
     * Sets the value of userIdentification.
     *
     * @param mixed $userIdentification the user identification
     *
     * @return self
     */
    public function setUserIdentification($userIdentification)
    {
        $this->userIdentification = $userIdentification;

        return $this;
    }

    /**
     * Gets the value of message.
     *
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Sets the value of message.
     *
     * @param mixed $message the message
     *
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Gets the value of type.
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value of type.
     *
     * @param mixed $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the value of comment.
     *
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Sets the value of comment.
     *
     * @param mixed $comment the comment
     *
     * @return self
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Gets the value of instructionId.
     *
     * @return mixed
     */
    public function getInstructionId()
    {
        return $this->instructionId;
    }

    /**
     * Sets the value of instructionId.
     *
     * @param mixed $instructionId the instruction id
     *
     * @return self
     */
    public function setInstructionId($instructionId)
    {
        $this->instructionId = $instructionId;

        return $this;
    }

    /**
     * Gets the value of bindings.
     *
     * @return mixed
     */
    public function getBindings()
    {
        return $this->bindings;
    }

    /**
     * Sets the value of bindings.
     *
     * @param mixed $bindings the bindings
     *
     * @return self
     */
    public function setBindings($bindings)
    {
        $this->bindings = $bindings;

        return $this;
    }
}