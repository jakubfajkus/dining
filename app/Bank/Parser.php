<?php namespace App\Bank;

/**
* 					
*/
class Parser 
{
	protected $data;
	protected $transactions = [];

	function __construct($data)
	{
		$this->data = $data;
	}


	public function parse()
	{
		//iterate over data
		foreach ($this->data->accountStatement->transactionList->transaction as $rawTransaction) 
		{
			$transaction = new BankTransaction;

			foreach ($rawTransaction as $column) 
			{
				if( is_null($column) )
				{
					continue;
				}

				//it is transaction type
				if($column->id == 8)
				{
					//if the type is not incoming payment
					//TESTED: works fine with czech diacritics
					if(! in_array($column->value, (new BankConfig())->getIncomingBankTransferTypes() ) )
					{
						break;
					}
				}
				


				$method = 'set' . BankTransaction::$bindings[ $column->id ];
				$transaction->$method( $column->value );
			}
			$this->transactions[] = $transaction;
		}
	}

	public function getReceivedPayments()
	{
		return $this->transactions;   
	}
}