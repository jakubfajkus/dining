<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\Routing\Middleware;

class DBSwitchingMiddleware implements Middleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$id = \Session::get('diningRoomId', 0);

		if($id)
		{
			$dbSwitcher = new \App\Helpers\DBSwitcher($id , \Config::get('database.diningRoomsInfoFolder') ) ;
			$dbSwitcher->switchDB();
		}		

		return $next($request);
	}

}
