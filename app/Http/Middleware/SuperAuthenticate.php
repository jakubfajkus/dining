<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\Middleware;
use Session;
use Config;
use DB;

class SuperAuthenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(Session::get('isMaster', false))
		{
			Config::set('database.default', 'master');
			DB::reconnect();

			if(! $request->user() )
			{
				return redirect()->guest('auth/login')->withErrors(['auth' => 'Přihlašte se prosím']);
			}
			if(! ($request->user()->role == Config::get('app.super_user_role_code')) )
			{
				return redirect()->guest('auth/login')->withErrors(['auth' => 'Nemáte přístup do admin sekce']);
			}
		}
		else
		{
			return redirect()->guest('auth/login')->withErrors(['auth' => 'Nemáte přístup do admin sekce']);
		}
		
		return $next($request);
	}

}
