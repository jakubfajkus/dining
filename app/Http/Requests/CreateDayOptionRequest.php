<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateDayOptionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return \Auth::user()->isAdmin();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		// get IDs of mealTypes avaiable for this meal
		$ids = [];
		foreach ( \App\Meal::find( $this->get('meal_id') )->mealTypes()->get(['id'])->all() as $mealType) 
		{
			$ids[] = $mealType->id;
		}

		return [
			'date'		 	=> 'required|after:' . date('d.m.Y'),
			'meal_id' 	 	=> 'required',
			'meal_type_id' 	=> 'required|in:' . implode(',' , $ids)
		];
	}

	/**
	 * Get the sanitized input for the request.
	 *
	 * @return array
	 */
	public function sanitize()
	{
		return $this->all();
	}

}
