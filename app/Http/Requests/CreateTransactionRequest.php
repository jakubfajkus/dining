<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateTransactionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return \Auth::user()->isAdmin();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'user_id' 				=> 'required',
			'transaction_amount'	=> 'required',
			'transaction_type_id' 	=> 'required'
		];
	}

}
