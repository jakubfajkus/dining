<?php 
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/' , function()
{
	return View::make('index')->with('loggedIn' , Auth::check())->with('isAdmin' ,(\Auth::user())? \Auth::user()->isAdmin() : false);	
	// dd(\Config::all());
	// \App\Helpers\DBSwitcher::switchDB(2);
	// App\User::create(
	// 	['name' 	=> 'honzik2',
	// 	 'email'	=> 'honzik2@lachtan.cz',
	// 	 'password' => Hash::make('pass'),
	// 	 'city'		=> 'mesto',
	// 	 'zip'		=> '7645',
	// 	 'street'	=> '1234 d',
	// 	 'comment'	=> 'BFU'
	// 	]
	// );
});	

Route::get('/test', function()
{
	// $helper = new \App\Helpers\DatabaseHelper();
	// $id = $helper->getLastDatabaseId();
	// $helper->createDatabase('dining_' . $id+1);
	// dd('dd');

	// for ($i=1; $i < 10; $i++) { 
		// DB::connection('master')->statement('drop database dining_'.$i);
	// }
});


$router->group(['prefix' => 'master' , 'middleware' => 'App\Http\Middleware\SuperAuthenticate'] , function($router)
{
	$router->resource('diningRoom' , 'MasterController');
});

$router->controller('auth' 	      , 'Auth\AuthController');
$router->controller('password'    , 'Auth\PasswordController');

$router->group(['prefix' => 'admin' , 'middleware' => 'App\Http\Middleware\AdminAuthenticate'],  function($router)
{
	Route::get('/',['as' => 'admin' , function()
	{
		return View::make('admin.home', ['user' => Auth::user() , 'isAdmin' => Auth::user()->isAdmin() , 'diningRoomId' => Session::get('diningRoomId', '')]);
	}]);

	$router->resource('mealType' 	  	, 'MealTypeController');
	$router->resource('user'     	  	, 'UserController');
	$router->resource('meal'     	  	, 'MealController');
	$router->resource('dayOption'     	, 'DayOptionController');
	$router->resource('transaction' 	, 'TransactionController');
	$router->resource('transactionType' , 'TransactionTypeController');
	$router->resource('order' 			, 'AdminOrderController');
	$router->get(	  'dayOptionHistory', [ 'as' => 'admin.dayOptionHistory.index'  , 'uses' => 'DayOptionController@indexHistory' ]);
	$router->get(	  'orderHistory'	, [ 'as' => 'admin.orderHistory.index' 		, 'uses' => 'AdminOrderController@indexHistory']);
	$router->post(	  'order'  			, 'AdminOrderController@orderPicked');
});


$router->group(['prefix' => 'user' , 'middleware' => 'App\Http\Middleware\Authenticate'] , function($router)
{
	Route::get('/' ,['as' => 'user' , function()
	{
		return View::make('user.home')->with('user' , \Auth::user() )->with('diningRoomId' , Session::get('diningRoomId', ''));
	}]);

	$router->resource('order' 			, 'UserOrderController');
	$router->resource('dayOption' 		, 'UserDayOptionController');
	$router->get('meal/{meal}'	, [ 'as' => 'user.meal.show' , 'uses' => 'UserMealController@show']);
	$router->get('meal/'	, [ 'as' => 'user.meal.index' , 'uses' => 'UserMealController@index']);
});


/*
|--------------------------------------------------------------------------
| Authentication & Password Reset Controllers
|--------------------------------------------------------------------------
|
| These two controllers handle the authentication of the users of your
| application, as well as the functions necessary for resetting the
| passwords for your users. You may modify or remove these files.
|
*/

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);



