<?php namespace App\Http\Controllers;

use App\Day;
use App\Meal;
use App\MealType;
use App\DayOption;
use View;
use Redirect;
use App\Http\Requests;
use App\Helpers\DayHelper;
use App\Http\Controllers\Controller; 
use App\Exceptions\CanNotDeleteException;


class DayOptionController extends Controller {

	protected $meal;
	protected $mealType;
	protected $day;
	protected $dayOption;

	protected $dayHelper;

	public function __construct(DayOption $dayOption , Meal $meal , MealType $mealType , Day $day , DayHelper $dayHelper)
	{	
		$this->dayOption = $dayOption;
		$this->meal = $meal;
		$this->mealType = $mealType;
		$this->day = $day;

		$this->dayHelper = $dayHelper;

		$this->middleware('App\Http\Middleware\AdminAuthenticate');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$days = $this->day->with(['dayOptions' , 'dayOptions.meal' , 'dayOptions.mealType' , 'dayOptions.orders'])
			->whereRaw("date >= date_format(now(),'%Y-%m-%d')") // we want select only that dates, which date is today or greater
			->orderBy('date')
			->get();
		$days = $this->dayHelper->deleteDaysWithoutDayOption($days);

		foreach ($days as $day)
		{
			$day->formatToCzechDate();
			$day->setCzechNameOfDay();

			foreach ($day->dayOptions as $dayOption) 
			{
				$dayOption->countOfOrders = $dayOption->orders()->count();
			}
		}

		return View::make('admin.dayOptions.index' , compact('days'));
	}

	/**
	 * Display a listing of the resource history records.
	 *
	 * @return Response
	 */
	public function indexHistory()
	{
		$days = $this->day->with(['dayOptions' , 'dayOptions.meal' , 'dayOptions.mealType' , 'dayOptions.orders'])
			->orderBy('date')
			->get();
		$days = $this->dayHelper->deleteDaysWithoutDayOption($days);

		foreach ($days as $day)
		{
			$day->formatToCzechDate();
			$day->setCzechNameOfDay();

			foreach ($day->dayOptions as $dayOption) 
			{
				$dayOption->countOfOrders = $dayOption->orders()->count();
			}
		}

		return View::make('admin.dayOptions.history.index' , compact('days'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$dayOption = $this->dayOption;
		$mealTypes = $this->mealType->orderBy('name')->get(['id','name'] );
		$meals = $this->meal->orderBy('name')->get();

		$modifiedMealTypes = [];
		$modifiedMeals = [];

		foreach ($mealTypes as $mealType) 
		{
			$modifiedMealTypes[$mealType->id] = $mealType->name;
		}

		foreach ($meals as $meal) 
		{
			$modifiedMeals[$meal->id] = $meal->name;
		}

		$mealTypes = $modifiedMealTypes;
		$meals = $modifiedMeals;
		$action = 'DayOptionController@store';
		$method = 'POST';

		return View::make('admin.dayOptions.create' , compact('dayOption' , 'mealTypes' , 'meals' , 'action' , 'method'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(\App\Http\Requests\CreateDayOptionRequest $request)
	{
		$this->dayOption->create([
			'date' 			=> $request->input('date') ,
			'meal_id' 		=> $request->input('meal_id') ,
			'meal_type_id' 	=> $request->input('meal_type_id') ,
			'comment' 		=> $request->input('comment') 
		]);

		return Redirect::to('admin/dayOption');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$dayOption = $this->dayOption->with('day' , 'meal')->find($id);
		$mealTypes = $this->mealType->orderBy('name')->get( ['id','name'] );
		$meals = $this->meal->orderBy('name')->get();

		$modifiedMealTypes = [];
		$modifiedMeals = [];

		foreach ($mealTypes as $mealType) 
		{
			$modifiedMealTypes[$mealType->id] = $mealType->name;
		}

		foreach ($meals as $meal) 
		{
			$modifiedMeals[$meal->id] = $meal->name;
		}

		$mealTypes = $modifiedMealTypes;
		$meals = $modifiedMeals;

		$action = 'DayOptionController@update';
		$method = 'PATCH';

		return View::make('admin.dayOptions.edit', compact('dayOption' , 'action' , 'method' , 'meals' , 'mealTypes') );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id 
	 * @return Response
	 */
	public function update(\App\Http\Requests\CreateDayOptionRequest $request , $id)
	{		
		$this->dayOption->find($id)->update([
			'date' 			=> $request->get('date') , 
			'meal_id' 		=> $request->get('meal_id') , 
			'meal_type_id' 	=> $request->get('meal_type_id') , 
			'comment' 		=> $request->get('comment')	
		]);		

		return Redirect::route('admin.dayOption.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try 
		{
			$this->dayOption->find($id)->delete();	
		} 
		catch (CanNotDeleteException $e) 
		{
			return Redirect::back()->withErrors(['can_not_delete' => 'Nelze odstranit. Již existují objednávky na tuto volbu']);
		}

		return Redirect::to('admin.dayOption.index');
	}

}
