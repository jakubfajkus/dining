<?php namespace App\Http\Controllers;

use App\Day;
use App\Order;
use App\MealType;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Collection;

class AdminOrderController extends Controller {

	protected $order;

	public function __construct(Order $order , Day $day , MealType $mealType)
	{
		$this->order 	= $order;
		$this->day 		= $day;
		$this->mealType = $mealType;
		$this->middleware('App\Http\Middleware\AdminAuthenticate');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//in debug mode, select all orders
		if (Config::get('app.debug') )
		{
			$days = $this->day
					->with( ['dayOptions' , 'dayOptions.meal' , 'dayOptions.mealType' , 'orders' ])
					->orderBy('date','DESC')
					->get();
		}
		else
		{
			$days = $this->day
					->whereRaw("date = date_format(now(),'%Y-%m-%d')")
					->with( ['dayOptions' , 'dayOptions.meal' , 'dayOptions.mealType' , 'orders' ])
					->first();
		}
		
		$mealTypesCollection = $this->mealType->all(); 

		$mealTypes = [];
		$mealTypes[0] = '';
		foreach ($mealTypesCollection as $mealType) 
		{
			$mealTypes[$mealType->name] = $mealType->name;
		}

		//Blade's forelse would throw an error, if $days is null
		if(is_null($days))
		{
			$days = new Collection();
		}

		return View::make( 'admin.orders.index' , compact('days' , 'mealTypes') );
	}

	/**
	 * Display a listing of the resource history records.
	 *
	 * @return Response
	 */
	public function indexHistory()
	{
		$orders = $this->order->with( 'dayOption' ,
									  'dayOption.meal' , 
									  'dayOption.mealType' , 
									  'dayOption.day' , 
									  'user' )
							  ->get();

		foreach ($orders as $order) 
		{	
			$order->dayOption->day->formatToCzechDate();
			$order->dayOption->day->setCzechNameOfDay();
		}

		return View::make( 'admin.orders.history.index' , compact('orders') );
	}

	public function orderPicked(Request $request)
	{
		if($request->ajax())
		{
			if ( Session::token() != Input::get( '_token' ) ) 
			{
	            return Response::json([
	                'msg' => 'Unauthorized attempt to pick order'
	            ]);
	        }
	 
			$this->order->find($request->input('id'))->setPicked();
	 
	        $response = [
	            'status' => 'success',
	            'msg' => 'Order picked successfully'
	        ];	        
	 
	        return Response::json($response);
		}
		else
		{
			$this->order->find($request->input('id'))->setPicked();
		}


		return Redirect::back();
	}

}
