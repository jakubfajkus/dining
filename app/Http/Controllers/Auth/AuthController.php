<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests\LoginUserRequest;
use App\Helpers\DBSwitcher;
use \View;
use \Auth;
use \Session;
use \Redirect;
use \Request;
use \Config;
use \DB;


class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	public function getLogin()
	{
		return View::make('auth.login');
	}

	public function getLogout()
	{
		Auth::logout();
		Session::clear();

		return Redirect::to('/');
	}

	public function postLogin(LoginUserRequest $request)
	{		
		$diningRoomCode = Request::get('dining_room_id');

		//if it is an secret code
		if(Config::get('app.super_user_code') == $diningRoomCode)
		{
			//authenticate the superUser to master database
			Config::set('database.default', 'master');
			DB::reconnect();

			if(Auth::attempt(['id' => Request::get('id') , 'password' => Request::get('password')]))
			{
				Session::set( 'isMaster', true );
				return Redirect::to('master/diningRoom');
			}
			else
			{
				return $this->userNotFound();
			}		
		}

		$dbSwitcher = new DBSwitcher($diningRoomCode , config('database.diningRoomsInfoFolder') ) ;

		if ( ! $dbSwitcher->switchDB() )
		{
			//fail
			return Redirect::back()->withErrors(['dining_room_id' => 'Jídelna nenalezena'])->withInput();
		}

		Session::set('diningRoomId', Request::get('dining_room_id'));

		if(Auth::attempt(['id' => Request::get('id') , 'password' => Request::get('password')]))
		{
			if(Auth::user()->isAdmin())
			{
				return Redirect::intended('admin');
			}
			else
			{
				return Redirect::intended('user');
			}
		}
		else
		{
			return $this->userNotFound();
		}
	}

	private function userNotFound()
	{
		return Redirect::to('auth/login')->withErrors(['userDoesNotExist' => 'Jméno nebo heslo neexistuje'])->withInput();
	}
}
