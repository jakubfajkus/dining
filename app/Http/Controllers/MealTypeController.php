<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Routing;
use App\MealType;
use App\Meal;
use App\Http\Requests\CreateMealTypeRequest;
use Illuminate\Support\Facades\Bus;
use App\Commands\CreateMealType;
use App\Commands\UpdateMealType;
use App\Commands\DeleteMealType;

use App\Exceptions\CanNotDeleteException;

class MealTypeController extends Controller {

	protected $mealType;

	public function __construct(MealType $mealType)
	{	
		$this->mealType = $mealType;

		$this->middleware('App\Http\Middleware\AdminAuthenticate');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$mealTypes = $this->mealType->all();

		return View::make('admin.mealTypes.index')->with('mealTypes' ,$mealTypes);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$mealType = $this->mealType;
		$action = 'MealTypeController@store';
		$method = 'POST';

		return View::make('admin.mealTypes.create_edit', compact('mealType' , 'action' , 'method') );		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateMealTypeRequest $request)
	{
		$this->mealType->create(['name' => $request->input('name')]);

		return Redirect::route('admin.mealType.index');		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$mealType = $this->mealType->find($id);

		return View::make('admin.mealTypes.show' , ['mealType' => $mealType , 'meals' => $mealType->meals]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$mealType = $this->mealType->find($id);
		$action = 'MealTypeController@update';
		$method = 'PATCH';

		return View::make('admin.mealTypes.create_edit')->with(compact('mealType' , 'action' , 'method'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CreateMealTypeRequest $request , $id)
	{		
		$this->mealType->find($id)->update(['name' => $request->input('name')]);

		return Redirect::route('admin.mealType.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try 
		{
			$this->mealType->find($id)->delete();
		} 
		catch (CanNotDeleteException $e) 
		{
			if($e->getCode() == 1)
			{
				return \Redirect::back()->withErrors(['can_not_delete' => 'Nelze odstranit. Již existují dny, na které je vypsáno jídlo tohoto typu.']);
			}
			else
			{
				return \Redirect::back()->withErrors(['can_not_delete' => 'Nelze odstranit. Již existují jídla tohoto typu.']);
			}
		}

		return Redirect::route('admin.mealType.index');
	}

}