<?php namespace App\Http\Controllers;

use View;
use Redirect;
use App\MealType;
use App\Meal;
use App\Http\Requests\CreateMealRequest;
use App\Http\Controllers\Controller;
use App\Exceptions\CanNotDeleteException;

class MealController extends Controller {

	protected $meal;
	protected $mealType;

	public function __construct(Meal $meal , MealType $mealType)
	{	
		$this->meal = $meal;
		$this->mealType = $mealType;

		$this->middleware('App\Http\Middleware\AdminAuthenticate');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$meals = $this->meal->all();
		
		return View::make('admin.meals.index')->with('meals' ,$meals);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$meal = $this->meal;
		$mealTypes = $this->mealType->all();
		$action = 'MealController@store';
		$method = 'POST';

		return View::make('admin.meals.create_edit', compact('meal' , 'mealTypes' ,'action' , 'method') );		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateMealRequest $request)
	{
		$mealTypesInput = $request->input('mealType');

		$mealTypes = [];
		for ($i = 0; $i < count($mealTypesInput) ; $i++) 
		{ 
			$mealType = $this->mealType->where('name' , 'like' , $mealTypesInput[$i])->first();
			$mealTypes[] = $mealType->id;
		}		

		// Bus::dispatch( new CreateMeal( $request->input('name') , $request->input('price') , $request->input('recipe') , $request->input('comment') , $mealTypes) );

		$this->meal->create([
			'name' 	  	=> $request->input('name'),
			'price'   	=> $request->input('price'),
			'recipe'  	=> $request->input('recipe'),
			'comment' 	=> $request->input('comment'),
			'mealTypes' => $mealTypes	
		]);

		return Redirect::route('admin.meal.index');		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$meal = $this->meal->with('dayOptions' , 'dayOptions.orders')->find($id);

		//count all dayOptions
		$meal->countOfDayOptions = $meal->countOfDayOptions();

		//count all orders
		$meal->countOfOrders = 0;
		foreach ($meal->dayOptions as $dayOption) 
		{
			$meal->countOfOrders = $meal->countOfOrders();
		}
	
		return View::make('admin.meals.show' , ['meal' => $meal , 'mealTypes' => $meal->mealTypes]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$meal = $this->meal->with('mealTypes')->find($id);
		$mealTypes = $this->mealType->all();

		foreach ($mealTypes as $mealType)
		{
			foreach ($meal->mealTypes as $mealsMealType)
			{
				if($mealType->id == $mealsMealType->id)
				{
					$mealType->checked = true;
				}
			}
		}

		$action = 'MealController@update';
		$method = 'PATCH';
		return View::make('admin.meals.create_edit')->with(compact('meal' , 'action' , 'method' , 'mealTypes'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */      
	public function update(CreateMealRequest $request , $id)
	{		
		$mealTypesInput = $request->input('mealType');

		$mealTypes = [];
		for ($i = 0; $i < count($mealTypesInput) ; $i++) 
		{ 
			$mealTypes[] = $this->mealType->where('name' , 'like' , $mealTypesInput[$i])->first()->id;
		}	

		$meal = $this->meal->find($id);
		$meal->update([
			'name' 	  	=>$request->input('name'),
			'price'   	=>$request->input('price'),
			'recipe'  	=>$request->input('recipe'),
			'comment' 	=>$request->input('comment'),
			'mealTypes' => $mealTypes	
		]);

		return Redirect::route('admin.meal.index');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try 
		{
			$this->meal->find($id)->delete();
		} 
		catch (CanNotDeleteException $e) 
		{
			return Redirect::back()->withErrors(['can_not_delete' => 'Nelze odstranit. Jídlo je již vypsáno na některý den.']);
		}

		return Redirect::route('admin.meal.index');
	}

}
