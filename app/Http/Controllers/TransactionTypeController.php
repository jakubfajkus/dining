<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTransactionTypeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Redirect;
use App\Commands\CreateTransactionType;
use App\Commands\UpdateTransactionType;
use App\Commands\DeleteTransactionType;
use App\Exceptions\CanNotDeleteException;

class TransactionTypeController extends Controller {

	private $transactionType;

	public function __construct(\App\TransactionType $transaction)
	{
		$this->transactionType = $transaction;

		$this->middleware('App\Http\Middleware\AdminAuthenticate');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$transactionTypes = $this->transactionType->all();

		return \View::make('admin.transactionTypes.index', compact('transactionTypes') );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$transactionType = $this->transactionType;
		$action = 'TransactionTypeController@store';
		$method = 'POST';

		return \View::make('admin.transactionTypes.create_edit', compact('transactionType' , 'action' , 'method'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateTransactionTypeRequest $request)
	{
		$this->transactionType->create(['name' => $request->input('name')]);

		return Redirect::route('admin.transactionType.index');	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$transactionType = $this->transactionType->find($id);
		$action = 'TransactionTypeController@update';
		$method = 'PATCH';

		return \View::make('admin.transactionTypes.create_edit')->with(compact('transactionType' , 'action' , 'method'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CreateTransactionTypeRequest $request , $id)
	{
		$this->transactionType->find($id)->update(['name' => $request->input('name')]);

		return Redirect::route('admin.transactionType.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try 
		{
			$this->transactionType->find($id)->delete();
		} 
		catch (CanNotDeleteException $e) 
		{
			Redirect::back()->withErrors( ['can_not_delete' => 'Nelze odstranit. Již existují transakce tohoto typu.' ]);
		}

		return Redirect::route('admin.transactionType.index');
	}

}
