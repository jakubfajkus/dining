<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTransactionRequest;
use Illuminate\Support\Facades\Bus;
use App\Commands\CreateTransaction;
use App\Commands\UpdateTransaction;
use App\Commands\DeleteTransaction;

use Illuminate\Http\Request;

class TransactionController extends Controller {

	protected $transaction;
	protected $transactionType;

	public function __construct(\App\Transaction $transaction , \App\TransactionType $transactionType , \App\User $user)
	{	
		$this->transaction = $transaction;
		$this->transactionType = $transactionType;
		$this->user = $user;

		$this->middleware('App\Http\Middleware\AdminAuthenticate');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$transactions = $this->transaction->with('user' , 'transactionType')->get();

		return \View::make('admin.transactions.index')->with('transactions' ,$transactions);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$transaction = $this->transaction;
		$transactionTypes = $this->transactionType->all();
		$users = $this->user->all();
		$transactionTypes = $this->transactionType->all();
		$action = 'TransactionController@store';
		$method = 'POST';
		$modifiedUsers = [];
		$modifiedTransactionTypes = [];

		foreach ($users as $user) 
		{
			$modifiedUsers[$user->id] = $user->email;
		}

		foreach ($transactionTypes as $transactionType) 
		{
			$modifiedTransactionTypes[$transactionType->id] = $transactionType->name;
		}

		$users = $modifiedUsers;
		$transactionTypes = $modifiedTransactionTypes;
		return \View::make('admin.transactions.create_edit', compact('transaction' , 'transactionTypes' ,'action' , 'method' , 'users') );		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateTransactionRequest $request)
	{
		$transaction = $this->transaction->create([
			'user_id' 				=> $request->input('user_id'),
			'transaction_amount' 	=> $request->input('transaction_amount'),
			'transaction_type_id' 	=> $request->input('transaction_type_id'), 
			'comment' 				=> $request->input('comment') 
		]);
		
		return \Redirect::route('admin.transaction.index');		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$transaction = $this->transaction->find($id);
		return \View::make('admin.transactions.show' , ['transaction' => $transaction , 'transactionTypes' => $transaction->transactionTypes]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$transaction = $this->transaction->find($id);
		$transactionTypes = $this->transactionType->all();
		$users = $this->user->all();
		$transactionTypes = $this->transactionType->all();
		$action = 'TransactionController@update';
		$method = 'PATCH';

		$modifiedUsers = [];
		$modifiedTransactionTypes = [];

		foreach ($users as $user) 
		{
			$modifiedUsers[$user->id] = $user->email;
		}

		foreach ($transactionTypes as $transactionType) 
		{
			$modifiedTransactionTypes[$transactionType->id] = $transactionType->name;
		}

		$users = $modifiedUsers;
		$transactionTypes = $modifiedTransactionTypes;
		return \View::make('admin.transactions.create_edit')->with(compact('transaction' , 'action' , 'method' , 'transactionTypes' , 'users'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update(CreateTransactionRequest $request , $id)
	{		
		$transaction = $this->transaction->find($id);
		$transaction->update([
			'user_id' 				=> $request->input('user_id'),
			'transaction_amount'	=> $request->input('transaction_amount'),
			'transaction_type_id'	=> $request->input('transaction_type_id'), 
			'comment' 				=> $request->input('comment') 	
		]);

		return \Redirect::route('admin.transaction.index');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$transaction = $this->transaction->find($id);
		$transaction->delete();
		
		return \Redirect::route('admin.transaction.index');
	}

}
