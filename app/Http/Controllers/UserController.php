<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User as User;
use App\Http\Requests\CreateUserRequest as CreateUserRequest;
use Illuminate\Support\Facades\Bus;
use App\Exceptions\CanNotDeleteException;
use App\Exceptions\CanNotCreateException;

use App\Commands\CreateUser;
use App\Commands\UpdateUser;
use App\Commands\DeleteUser;

class UserController extends Controller {

	protected $user;

	public function __construct(User $user)
	{	
		$this->user = $user;
		
		$this->middleware('App\Http\Middleware\AdminAuthenticate');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = $this->user->all();
		
		return \View::make('admin.users.index')->with('users' ,$users);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = $this->user->getModel();
		$isAdmin = $user->isAdmin();
		$action = 'UserController@store';
		$method = 'POST';

		return \View::make('admin.users.create_edit', compact('user' , 'action' , 'method' , 'isAdmin') );	
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateUserRequest $request)
	{
		$this->user->create([
			'email' 	=> $request->get('email'),
			'password' 	=> $request->get('password'),
			'city' 		=> $request->get('city'),
			'zip' 		=> $request->get('zip'),
			'street' 	=> $request->get('street'),
			'comment' 	=> $request->get('comment'),
			'name' 		=> $request->get('name'),
			'admin' 	=> $request->get('admin')
		]);				

		return \Redirect::route('admin.user.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = $this->user->find($id);

		return \View::make('admin.users.show')->with('user' , $user);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->user->find($id);
		$isAdmin = $user->isAdmin();
		$user->password = '';
		$action = 'UserController@update';
		$method = 'PATCH';

		return \View::make('admin.users.create_edit')->with(compact('user' , 'action' , 'method' , 'isAdmin'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CreateUserRequest $request , $id)
	{
		$this->user->find($id)->update([
			'email' 	=> $request->get('email'),
			'password' 	=> $request->get('password'),
			'city' 		=> $request->get('city'),
			'zip' 		=> $request->get('zip'),
			'street' 	=> $request->get('street'),
			'comment' 	=> $request->get('comment'),
			'name' 		=> $request->get('name'),
			'admin' 	=> $request->get('admin')	
		]);

		return \Redirect::route('admin.user.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try 
		{
			$this->user->find($id)->delete();
		} 
		catch (CanNotDeleteException $e) 
		{
			if($e->getCode() == 1)
			{
				return \Redirect::back()->withErrors(['can_not_delete' => 'Nelze odstranit. Uživatel má jíž přiřazené transakce.']);
			}
			else
			{
				return \Redirect::back()->withErrors(['can_not_delete' => 'Nelze odstranit. Uživatel má jíž vytvořené objednávky.']);
			}
		}

		return \Redirect::route('admin.user.index');
	}

}
