<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\DayOptionHelper;
use App\Helpers\DayHelper;

class UserDayOptionController extends Controller {

	public function __construct(\App\Transaction $transaction , \App\DayOption $dayOption , DayOptionHelper $dayOptionHelper , DayHelper $dayHelper)
	{
		$this->transaction = $transaction;
		$this->dayOption = $dayOption;
		$this->dayOptionHelper = $dayOptionHelper;
		$this->dayHelper = $dayHelper;

		$this->middleware('App\Http\Middleware\Authenticate');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userId = \Auth::user()->id;

		//get all days with day options and meals, order by date, chaining eager loading
		//get all orders from logged user
		$days = \App\Day::with(['dayOptions' , 'dayOptions.meal' , 'orders' => function($query)
		{
			$query->where('user_id' , '=' , \Auth::user()->id ); 
		}])
			->whereRaw("date >= date_format(now(),'%Y-%m-%d')") // we want select only that dates, which date is today or greater
			->orderBy('date')
			->get();

		foreach ($days as $day)
		{
			$day->formatToCzechDate();
			$day->setCzechNameOfDay();
		}

		$days = $this->dayHelper->deleteDaysWithoutDayOption($days);
		//find out, if the dayOption was ordered by user
		$days = $this->dayOptionHelper->setOrderedAttributes($days);


		return \View::make('user.dayOptions.index' , compact('days' , 'userId'));		
	}
	
}

