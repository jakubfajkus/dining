<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\UserCreateOrderRequest;
use Illuminate\Support\Facades\Bus;

use App\User;
use App\DayOption;
use App\Day;
use App\Order;

use App\Helpers\DayHelper;
use App\Helpers\DayOptionHelper;

use Auth;
use View;
use Redirect;

class UserOrderController extends Controller {

	protected $order;
	protected $day;
	protected $dayHelper;

	public function __construct( Order $order , Day $day, DayHelper $dayHelper, DayOptionHelper $dayOptionHelper)
	{
		$this->order = $order;
		$this->day = $day;
		$this->dayHelper = $dayHelper;
		$this->dayOptionHelper = $dayOptionHelper;

		$this->middleware('App\Http\Middleware\Authenticate');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userId = Auth::user()->id;

		//get all days with day options and meals, order by date, chaining eager loading
		//get all orders from logged user
		$days = $this->day->with(['dayOptions' , 'dayOptions.meal' , 'orders' => function($query)
		{
			$query->where('user_id' , '=' , Auth::user()->id ); 
		}])	->orderBy('date')
			->get();

		foreach ($days as $day)
		{
			$day->formatToCzechDate();
			$day->setCzechNameOfDay();
		}

		$days = $this->dayHelper->deleteDaysWithoutDayOption($days);

		//find out, if the dayOption was ordered by user
		$days = $this->dayOptionHelper->setOrderedAttributes($days);

		return View::make('user.orders.index' , compact('days' , 'userId'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserCreateOrderRequest $request)
	{		
		$this->order->create(['user' => Auth::user(), 'day_option_id' => $request->day_option_id]);

		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		$this->order->find($id)->delete();

		return Redirect::back();
	}

}
