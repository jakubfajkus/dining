<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserMealController extends Controller {

	public function __construct()
	{
		$this->middleware('App\Http\Middleware\Authenticate');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$meals = \App\Meal::all();
		return \View::make('user.meals.index' , compact('meals'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$meal 		= \App\Meal::find($id);
		$mealTypes  = $meal->mealTypes;
		return \View::make('user.meals.show' , compact('meal' , 'mealTypes'));
	}

}
