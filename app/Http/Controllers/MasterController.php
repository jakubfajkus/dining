<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDiningRoomRequest;
use App\Commands\CreateDiningRoom;
use App\Commands\UpdateDiningRoom;
use App\Commands\DeleteDiningRoom;
use Illuminate\Http\Request;

class MasterController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//switch to master DB and select all rows and columns
		$data = DB::connection('master')->table('dining_rooms')->select('*')->get();
		//render the view
		return View::make('master.diningRooms.index' , compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$action = 'MasterController@store';
		$method = 'POST';
		return View::make('master.diningRooms.create' , compact('action' , 'method'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateDiningRoomRequest $request)
	{
		$password = md5(rand());
		
		Bus::dispatch( new CreateDiningRoom(
			$request->get('name'),
			$request->get('city'),
			$request->get('zip'),
			$request->get('street'),
			$request->get('email'),
			$request->get('phone'),
			$request->get('host'),
			$password
		)) ;

		//switch to master DB and select all rows and columns
		//needs reset to default database name
		\Config::set('database.connections.master.database' , 'dining_master');

		$data 		  = DB::reconnect('master')->table('dining_rooms')->select('*')->get();
		$diningRoomId = DB::connection('master')->table('dining_rooms')->select('id')->orderBy('id' , 'DESC')->limit(1)->get()[0]->id;
		//render the view
		return View::make('master.diningRooms.index' , compact('data' , 'password' , 'diningRoomId'));
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$action = 'MasterController@update';
		$method = 'PATCH';

		$diningRoom = DB::connection('master')->table('dining_rooms')->select(['id' , 'name' , 'city' , 'zip' , 'street' , 'email' , 'telephone' , 'host'])->where('id' , '=' , $id)->first();
		
		return View::make('master.diningRooms.edit' , compact('action' , 'method' , 'diningRoom'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CreateDiningRoomRequest $request , $id)
	{
		Bus::dispatch(new UpdateDiningRoom(
			$id,
			$request->get('name'),
			$request->get('city'),
			$request->get('zip'),
			$request->get('street'),
			$request->get('email'),
			$request->get('phone'),
			$request->get('host')
		));

		return Redirect::action('MasterController@index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Bus::dispatch(new DeleteDiningRoom($id) );

		return Redirect::action('MasterController@index');
	}

}
