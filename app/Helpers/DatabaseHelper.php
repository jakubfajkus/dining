<?php namespace App\Helpers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use App\Commands\CreateUser;
	
	/**
	* Helper class for working with database		
	*/
	class DatabaseHelper
	{
		public function createDatabase($name)
		{
			DB::connection('master')->statement('CREATE DATABASE ' . $name . ' COLLATE utf8_general_ci');
		}

		public function getLastDatabaseId()
		{
			$result = DB::connection('master')->select('SELECT last_dining_room_id FROM global');
			
			return $result[0]->last_dining_room_id;
		}

		public function setLastDatabaseId($id)
		{
			DB::connection('master')->update('UPDATE global SET last_dining_room_id = ?' , [$id]);
		}

		public function createUser($username , $password , $databaseName ,$host)
		{
			$sql = "CREATE USER '$username'@'$host' IDENTIFIED BY '$password';";
			DB::connection('master')->statement($sql);
			$sql = "GRANT SELECT, INSERT, UPDATE, DELETE ON `$databaseName`.* TO '$username'@'$host';";
			DB::connection('master')->statement($sql);
		}

		public function addRecordToDiningMaster($name ,$city , $zip , $street , $email, $phone, $host, $username, $password)
		{
			DB::connection('master')->table('dining_rooms')->insert([
				'name' 	=> $name,
				'city' 	=> $city,
				'zip' 	=> $zip,
				'street'=> $street,
				'email' => $email,
				'telephone' => $phone,
				'host' 	=> $host,
				'username' 	=> $username,
				'password' 	=> $password
			]);
		}

		public function migrateDatabase($name)
		{
			\Config::set('database.connections.master.database' , $name);
			DB::reconnect('master');
			Artisan::call('migrate', array('--database' => 'master'));
		}

		public function createFirstUser($email, $password , $city, $zip, $street, $name)
		{
			Bus::dispatch( new CreateUser($email , $password , $city , $zip , $street , $comment = null , $name , $adin = true) );
		}
	}

