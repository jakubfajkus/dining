<?php namespace App\Helpers;

use Illuminate\Support;
	
	/**
	* Helper class for working with days and dates		
	*/
	class DayOptionHelper
	{
		/**
		 * Set and attribute ordered to dayOption. Inout parameter has to have eager loaded dayOptions, orders and dayOptions.meal
		 * @param \Illuminate\Support\Collection $days 
		 */
		public function setOrderedAttributes(\Illuminate\Support\Collection $days)
		{
			//find out, if the dayOption was ordered by user
			foreach ($days as $day) 
			{
				foreach ($day->dayOptions as $dayOption) 
				{
					foreach ($dayOption->orders as $order) 
					{
						if($dayOption->id === $order->day_option_id)
						{
							if($order->user_id === \Auth::user()->id)
							{
								$dayOption->ordered = true;
							}
						}
					}
				}
			}

			return $days;
		}
	}

