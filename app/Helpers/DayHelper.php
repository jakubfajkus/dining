<?php namespace App\Helpers;

use Illuminate\Support;
	
	/**
	* Helper class for working with days and dates		
	*/
	class DayHelper
	{
		/**
		 * Delete days, which have no dayOptions, from given collection
		 * @param  \Illuminate\Support\Collection $days source days
		 * @return \Illuminate\Support\Collection modified collection
		 */
		public function deleteDaysWithoutDayOption(\Illuminate\Support\Collection $days)
		{
			$i = -1;

			foreach ($days as $day) 
			{
				$i++;

				if($day->dayOptions->isEmpty())
				{
					$days->forget($i);
					continue;
				}
			}
			return $days;
		}
	}

