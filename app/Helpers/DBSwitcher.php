<?php namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

/*
* Maintains switching between database connections
*/
class DBSwitcher
{
	protected $id;
	protected $folderName;

	public function __construct($id , $folderName)
	{
		$this->id 			= $id;
		$this->folderName 	= $folderName;
	}

	protected function createDBName()
	{
		$base = 'dining_';

		return $base . $this->id;
	}

	protected function loadFromFile($fileName)
	{
		// dd($this->folderName);
		$filePath = $this->folderName . '/' . $fileName;

		if( Storage::exists($filePath) )
		{
			return Storage::get($filePath);
		}
		// dd('no file found');
		return false;
	}

	protected function loadCredentials()
	{
		//get an hash from master database

		$result = DB::connection('master')->table('dining_rooms')->where('id' , '=' , $this->id)->first();
		if(! $result)
		{
			// dd('no dinong_room found');
			return false;
		}		
		$username = $result->username;
		$password = $result->password;
		$credentials = [];

		//find file with name of hash
		$credentials['username'] = $this->loadFromFile($username);
		$credentials['password'] = $this->loadFromFile($password);

		if(!$credentials['username'] || !$credentials['password'])	
		{
			return false;
		}

		//return credentials
		return $credentials;
	}

	public function switchDB()
	{
		$credentials = $this->loadCredentials();

		// dd($credentials);
		if(! $credentials || !$credentials['username'] || !$credentials['password'])
		{
			// dd('credintials not loaded');
			return false;
		}	

		$name = $this->createDBName();

		\Config::set('database.connections.dining_room.database' , $name);
		\Config::set('database.connections.dining_room.username' , $credentials['username']);
		\Config::set('database.connections.dining_room.password' , $credentials['password']);

		DB::reconnect(); 
		
		return true;
	}


}