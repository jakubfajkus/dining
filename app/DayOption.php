<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Exceptions\CanNotDeleteException;

class DayOption extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'day_options';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['day_id' , 'meal_id' , 'meal_type_id' , 'comment'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * Varaibles included in the toArray method
	 * @var [type]
	 */
	protected $appends = ['ordered'];

	public static function create(array $values = array())
	{
		$day = Day::firstOrCreate(['date' => $values['date']]);
		$values['day_id'] = $day->id;

		parent::create($values);
	}

	public function update(array $newValues = array())
	{
		$day = Day::firstOrCreate([
			'date' => $newValues['date']
		]);

		$newValues['day_id'] = $day->id;
		parent::update($newValues);
	}

	public function delete()
	{
		if($this->orders->isEmpty())
		{
			parent::delete();
		}
		else
		{
			throw new CanNotDeleteException();				
		}		
	}

	public function day()
	{
		return $this->belongsTo('\App\Day')->orderBy('date'   );
	}

	public function meal()
	{
		return $this->belongsTo('\App\Meal')->orderBy('name');
	}

	public function orders()
	{
		return $this->hasMany('\App\Order');
	}

	public function mealType()
	{
		return $this->belongsTo('\App\MealType')->orderBy('name');
	}
}