<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\Authenticator;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Exceptions\CanNotDeleteException;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract 
{

	public static $ADMIN_ROLE_CODE = 1;
	public static $USER_ROLE_CODE = 0;

	use Authenticatable, CanResetPassword,SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password' , 'city' , 'street' , 'zip' , 'comment'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token' , 'role'];

	public function delete()
	{
		if(! $this->transactions->isEmpty())
		{
			throw new CanNotDeleteException("Can not delete user. The user has already transactions.", 1);
			
		}
		else if(! $this->orders->isEmpty())
		{
			throw new CanNotDeleteException("Can not delete user. The user has already orders.", 2);
		}

		parent::delete();
	}

	public function setDiningRoomIdAttribute($id)
	{
	    return $this->attributes['diningRoomId'] = $id;
	}
	
	/**
	 * Add credit to the user.		
	 * @param  int,float,double $credit amount of credit to subtract
	 * @throws InvalidArgumentException If argument is not a number
	 */
	public function addCredit($credit)
	{
		if(is_integer($credit) || is_double($credit) || is_float($credit))
		{
			$this->credit += abs($credit);
			$this->save();
		}
		else
		{
			throw new \InvalidArgumentException("Method addCredit accepts only integers, floats and doubles, the input was: " . $credit);			
		}
	}

	/**
	 * Subtract credit from the user account.
	 * @param  int,float,double $credit amount of credit to subtract
	 * @throws InvalidArgumentException If argument is not a number
	 */
	public function subtractCredit($credit)
	{
		if(is_integer($credit) || is_double($credit) || is_float($credit))
		{
			$this->credit -= abs($credit);
			$this->save();
		}
		else
		{
			throw new \InvalidArgumentException("Method subtractCredit accepts only integers, floats and doubles, the input was: " . $credit);
		}
	}

	/**
	 * Check, if the user is admin or not
	 * @return boolean true - user is an admin, false otherwise
	 */
	public function isAdmin()
	{
		if($this->role === self::$ADMIN_ROLE_CODE)
			return true;
		else
			return false;
	}

	/**
	 * Set the user role to 1 - admin and save it
	 * @return [type] [description]
	 */
	public function beAdmin()
	{
		$this->role = self::$ADMIN_ROLE_CODE;
		$this->save();
	}

	/**
	 * Set the user role to 0 - user and save it
	 * @return [type] [description]
	 */
	public function beUser()
	{
		$this->role = self::$USER_ROLE_CODE;
		$this->save();		
	}

	public function transactions()
	{
		return $this->hasMany('App\Transaction');
	}

	public function orders()
	{
		return $this->hasMany('App\Order');
	}

}
