<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
	use SoftDeletes;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transactions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = 
	[
	 'id' ,
	 'user_id' ,
	 'transaction_amount' ,
	 'transaction_type_id' ,
	 'comment' ,
	 'account', 
	 'bank_code', 
	 'bank_name', 
	 'constant_symbol', 
	 'variable_symbol', 
	 'specific_symbol', 
	 'user_identification', 
	 'message', 
	 'type', 
	 'transaction_comment'
	];

	/*
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	protected $dates = ['deleted_at'];

	public static function create(array $newValues = array())
	{
		$user = User::find($newValues['user_id']);
		$user->addCredit((float)$newValues['transaction_amount']);

		parent::create($newValues);
	}

	public function update(array $newValues = array())
	{
		$oldUserId = $this->user_id;
		$newUserId = $newValues['user_id'];

		//subtract credit from the old user
		$oldUser = User::find($oldUserId);
		$oldUser->subtractCredit((float)$this->transaction_amount);

		if($oldUserId != $newUserId)
		{
			//add credit to the new user
			$newUser = User::find($newUserId);
			$newUser->addCredit((float)$newValues['transaction_amount']);
		}
		else
		{
			//add credit to the new user
			$oldUser->addCredit((float)$newValues['transaction_amount']);	
		}

		parent::update($newValues);
	}

	public function delete()
	{
		User::find($this->user_id)->subtractCredit($this->transaction_amount);

		parent::delete();
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function transactionType()
	{
		return $this->belongsTo('\App\TransactionType');
	}
}