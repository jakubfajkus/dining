<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Exceptions\CanNotDeleteException;

class Meal extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'meals';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name' , 'price' , 'recipe' , 'comment'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public static function create(array $values = array())
	{
		$newMeal = parent::create($values);

		$newMeal->mealTypes()->attach($values['mealTypes']);
	}

	public function update(array $newValues = array())
	{
		parent::update($newValues);

		$this->mealTypes()->detach();
		$this->mealTypes()->attach($newValues['mealTypes']);
	}

	public function delete()
	{
		if($this->dayOptions->isEmpty())
		{	
			$this->mealTypes()->detach();
			parent::delete();
		}
		else
		{
			throw new CanNotDeleteException();				
		}
	}

	public function mealTypes()
	{
		return $this->belongsToMany('App\MealType' , 'meals_meal_types');
	}

	public function dayOptions()
	{
		return $this->hasMany('App\DayOption');
	}

	public function days()
	{
		return $this->belongsToMany('App\Day' , 'day_options');
	}
	/**
	 * Checks, is the given mealType(or array of mealTypes) can be applied to this meal
	 * @param  Meal or Array(Meal)  $mealType Instance of Meal or array of instances. Meal types from user tovalidate.
	 * @return boolean           true, if meal type can be applied to this meal
	 */
	public function isTypeOfMealType($mealType)
	{
		//get mealTypes 
		$mealTypes = $this->mealTypes();
		
		if( is_array($mealType) )
		{
			foreach ($mealType as $oneMealType) 
			{
				foreach ($mealTypes as $mealType) 
				{
					if( $oneMealType === $mealType->name)
						return true;
				}	
			}
		}
		else
		{
			foreach ($mealTypes as $mealType) 
			{
				if( $mealType === $mealTypes->name)
					return true;
			}
		}

		return false;	
	}

	public function countOfDayOptions()
	{
		return $this->dayOptions()->count();
	}

	public function countOfOrders()
	{
		$countOfOrders = 0;
		foreach ($this->dayOptions as $dayOption) 
		{
			$countOfOrders += $dayOption->orders()->count();
		}

		return $countOfOrders;
	}
}