<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CanNotDeleteException;

class MealType extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'meal_types';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public function delete()
	{
		if(! $this->meals->isEmpty()) 
		{
			throw new CanNotDeleteException('Can not delete mealType. There is at least one meal, which depends on it.', 1);
		}
		if(! $this->dayOptions->isEmpty())
		{
			throw new CanNotDeleteException('Can not delete mealType. There is at least one dayOption, which depends on it.', 2);
		}

		parent::delete();		
	}

	public function meals()
	{
		return $this->belongsToMany('App\Meal' , 'meals_meal_types');
	}

	public function dayOptions()
	{
		return $this->hasMany('App\DayOption');
	}

}